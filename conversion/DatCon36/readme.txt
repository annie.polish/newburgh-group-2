DatCon version 2.0.0


   1. To install on Windows execute (double-click) the DatConSetup file. It should install
      and offer to start DatCon. The first time DatCon is run your anti-virus software will
      want to inspect DatCon. Also, on the first run, DatCon checks to see if you have Java
      version 1.7 or later. If not, you'll be required to install it.

   2. To see the user manual double-click the file "DatCon Usage.pdf". This is also
      accesible via the Help Menu in DatCon.

   3. DatCon.exe is included if you can't or don't want to run DatConSetup.

   4. If you have a Mac or Linux system you can try running DatCon by using DatCon.jar. This
      has been shown to work on both Mac and Linux systems but hasn't been extensively tested.
      Go to a command line interpreter (shell, bash, etc) and type

                  java -jar DatCon.jar
