EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_B_Micro J1
U 1 1 602BAA22
P 9550 800
F 0 "J1" V 9561 1130 50  0000 L CNN
F 1 "USB_B_Micro" V 9652 1130 50  0000 L CNN
F 2 "Connector_USB:USB_Micro-B_Amphenol_10118194_Horizontal" H 9700 750 50  0001 C CNN
F 3 "~" H 9700 750 50  0001 C CNN
	1    9550 800 
	0    1    1    0   
$EndComp
$Comp
L custom:OpenLog U2
U 1 1 602BC4EB
P 6000 4750
F 0 "U2" H 6378 5079 50  0000 L CNN
F 1 "OpenLog" H 6378 4988 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 6000 4750 50  0001 C CNN
F 3 "" H 6000 4750 50  0001 C CNN
	1    6000 4750
	0    -1   -1   0   
$EndComp
$Comp
L custom:PB1000C U6
U 1 1 602E450D
P 10350 1800
F 0 "U6" H 10778 2167 50  0000 L CNN
F 1 "PB1000C" H 10778 2076 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 10350 1800 50  0001 C CNN
F 3 "" H 10350 1800 50  0001 C CNN
	1    10350 1800
	0    -1   -1   0   
$EndComp
$Comp
L custom:TP_out U5
U 1 1 602E4BC9
P 1900 3950
F 0 "U5" H 2028 4292 50  0000 L CNN
F 1 "TP_out" H 2028 4201 50  0000 L CNN
F 2 "Connector_Coaxial:SMA_Amphenol_901-144_Vertical" H 1900 3950 50  0001 C CNN
F 3 "" H 1900 3950 50  0001 C CNN
	1    1900 3950
	0    1    1    0   
$EndComp
$Comp
L custom:XBee U3
U 1 1 602E50C9
P 6250 2950
F 0 "U3" H 6478 3267 50  0000 L CNN
F 1 "XBee" H 6478 3176 50  0000 L CNN
F 2 "custom:xbee" H 6250 2950 50  0001 C CNN
F 3 "" H 6250 2950 50  0001 C CNN
	1    6250 2950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 602E6A41
P 8650 950
F 0 "SW1" H 8650 1185 50  0000 C CNN
F 1 "SW_SPST" H 8650 1094 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8650 950 50  0001 C CNN
F 3 "~" H 8650 950 50  0001 C CNN
	1    8650 950 
	1    0    0    -1  
$EndComp
$Comp
L custom:qwiic U4
U 1 1 602EB24A
P 5900 3750
F 0 "U4" H 6128 4092 50  0000 L CNN
F 1 "qwiic" H 6128 4001 50  0000 L CNN
F 2 "Connector_JST:JST_SH_BM04B-SRSS-TB_1x04-1MP_P1.00mm_Vertical" H 5900 3750 50  0001 C CNN
F 3 "" H 5900 3750 50  0001 C CNN
	1    5900 3750
	0    -1   -1   0   
$EndComp
$Comp
L custom:neopixel_nooutput D1
U 1 1 6031854A
P 1500 4850
F 0 "D1" H 1730 4896 50  0000 L CNN
F 1 "neopixel_nooutput" H 1730 4805 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1550 4550 50  0001 L TNN
F 3 "https://www.adafruit.com/product/1938" H 1600 4475 50  0001 L TNN
	1    1500 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 1450 9750 1450
Wire Wire Line
	9750 1450 9750 1100
Wire Wire Line
	9150 1850 9150 800 
Wire Wire Line
	8850 950  8850 1750
Wire Wire Line
	8850 1750 10100 1750
Wire Wire Line
	8450 950  8450 1850
Wire Wire Line
	8450 1850 9150 1850
Connection ~ 9150 1850
Wire Wire Line
	9150 1850 10000 1850
Wire Wire Line
	700  5600 700  700 
Wire Wire Line
	700  700  8350 700 
Wire Wire Line
	8350 700  8350 2050
Wire Wire Line
	8350 2050 10000 2050
Wire Wire Line
	10100 2150 8250 2150
Wire Wire Line
	8250 2150 8250 800 
Wire Wire Line
	8250 800  6300 800 
Wire Wire Line
	3700 800  3700 1000
Wire Wire Line
	700  5600 3500 5600
Wire Wire Line
	5500 4850 5400 4850
Wire Wire Line
	5400 4850 5400 4950
Wire Wire Line
	5400 5600 3700 5600
Wire Wire Line
	5500 4550 5300 4550
Wire Wire Line
	5300 4550 5300 4600
Wire Wire Line
	5300 4600 5000 4600
Wire Wire Line
	5000 4650 5000 4700
Wire Wire Line
	5000 2900 5300 2900
Wire Wire Line
	5300 2900 5300 2800
Wire Wire Line
	5300 1250 2600 1250
Wire Wire Line
	2600 1250 2600 4600
Wire Wire Line
	2600 4600 2800 4600
Wire Wire Line
	6050 2800 5300 2800
Connection ~ 5300 2800
Wire Wire Line
	5300 2800 5300 1250
Wire Wire Line
	6050 2900 5700 2900
Wire Wire Line
	5700 2900 5700 2850
Wire Wire Line
	5000 2850 5000 2800
Wire Wire Line
	6050 3000 5600 3000
Wire Wire Line
	5350 3000 5350 1200
Wire Wire Line
	5350 1200 3600 1200
Wire Wire Line
	3600 1200 3600 1400
Wire Wire Line
	6050 3100 6050 5700
Wire Wire Line
	6050 5700 3600 5700
Wire Wire Line
	1500 5150 1500 5400
Wire Wire Line
	1500 4550 1500 1000
Wire Wire Line
	1500 1000 3700 1000
Connection ~ 3700 1000
Wire Wire Line
	3700 1000 3700 1400
Wire Wire Line
	1200 4850 1200 4500
Wire Wire Line
	1200 4500 2500 4500
Wire Wire Line
	2500 4500 2500 4700
Wire Wire Line
	2500 4700 2800 4700
Wire Wire Line
	5700 3800 6400 3800
Wire Wire Line
	6400 3800 6400 1200
Wire Wire Line
	6400 1200 5350 1200
Connection ~ 5350 1200
Wire Wire Line
	5700 3900 5400 3900
Wire Wire Line
	5400 3900 5400 4100
Connection ~ 5400 4850
Wire Wire Line
	5950 3100 5950 4100
Wire Wire Line
	5950 4100 5400 4100
Connection ~ 5400 4100
Wire Wire Line
	5500 4750 5300 4750
Wire Wire Line
	5300 4750 5300 5250
Wire Wire Line
	5300 5250 6300 5250
Wire Wire Line
	6300 5250 6300 800 
Connection ~ 6300 800 
Wire Wire Line
	6300 800  3700 800 
Connection ~ 3500 5400
Wire Wire Line
	3500 5400 3600 5400
Connection ~ 3600 5400
Wire Wire Line
	3600 5400 3700 5400
Connection ~ 3700 5400
Wire Wire Line
	3700 5400 3800 5400
Wire Wire Line
	10000 1850 10000 2050
Connection ~ 10000 1850
Wire Wire Line
	10000 1850 10100 1850
Connection ~ 10000 2050
Wire Wire Line
	10000 2050 10100 2050
Wire Wire Line
	5650 3800 5600 3800
Wire Wire Line
	5600 3800 5600 3000
Connection ~ 5600 3000
Wire Wire Line
	5600 3000 5350 3000
Connection ~ 3400 5400
Wire Wire Line
	3400 5400 3500 5400
Connection ~ 3300 5400
Wire Wire Line
	3300 5400 3400 5400
Wire Wire Line
	1500 5400 2350 5400
Wire Wire Line
	5500 4650 5000 4650
Wire Wire Line
	5700 2850 5000 2850
Wire Wire Line
	3700 5600 3700 5400
Wire Wire Line
	3600 5700 3600 5400
Wire Wire Line
	3500 5600 3500 5400
$Comp
L custom:NUCLEO64-modified U1
U 1 1 602D8E20
P 3900 3400
F 0 "U1" H 3900 1311 50  0000 C CNN
F 1 "NUCLEO64-modified" H 3900 1220 50  0000 C CNN
F 2 "custom:nucleo-64" H 4450 1500 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/data_brief/DM00105918.pdf" H 3000 2000 50  0001 C CNN
	1    3900 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4100 5400 4850
Wire Wire Line
	5700 3600 5250 3600
Wire Wire Line
	5250 3600 5250 4400
Wire Wire Line
	5250 4400 5000 4400
Wire Wire Line
	5700 3700 5350 3700
Wire Wire Line
	5350 3700 5350 4500
Wire Wire Line
	5350 4500 5000 4500
Wire Wire Line
	9150 700  9150 800 
Connection ~ 9150 800 
Wire Wire Line
	2150 4000 2450 4000
Wire Wire Line
	2450 4000 2450 4400
Wire Wire Line
	2450 4400 2800 4400
Wire Wire Line
	2150 3850 2350 3850
Wire Wire Line
	2350 3850 2350 5400
Connection ~ 2350 5400
Wire Wire Line
	2350 5400 3300 5400
Wire Wire Line
	5500 4950 5400 4950
Connection ~ 5400 4950
Wire Wire Line
	5400 4950 5400 5600
$EndSCHEMATC
