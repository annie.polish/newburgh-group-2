#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>



// maximum length of a UBX payload
#define MAX_UBX          128

// size of the packet buffer
#define BUF_SIZE         4

// the header string for the CSV file
//char head_str[] = "iTOW,UTC,nano,tAcc,ttff,msss,Lon,Lat,Height,HMSL,hAcc,vAcc,pDOP,numSV,velN,velE,velD,gspeed,headmot,sAcc,headAcc,LLHerr,UTCerr,fixType,fixOK,diffCorrected";

// structs for individual packets
typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint32_t iTOW;
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t min;
	uint8_t sec;
	uint8_t valid; //flags
	uint32_t tAcc;
	int32_t nano;
	uint8_t fixType;
	uint8_t flags;
	uint8_t flags2;
	uint8_t numSV;
	int32_t lon;
	int32_t lat;
	int32_t height;
	int32_t hMSL;
	uint32_t hAcc;
	uint32_t vAcc;
	int32_t velN;
	int32_t velE;
	int32_t velD;
	int32_t gSpeed;
	int32_t headMot;
	uint32_t sAcc;
	uint32_t headAcc;
	uint16_t pDOP;
	uint8_t flags3;
	uint8_t reserved[5];
	int32_t headVeh;
	int16_t magDec;
	uint16_t macAcc;
	uint8_t CK_A;
	uint8_t CK_B;
} PVT;

typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint8_t version;
	uint16_t reserved;
	uint8_t flags;
	uint32_t iTOW;
	int32_t lon;
	int32_t lat;
	int32_t height;
	int32_t hMSL;
	int8_t lonHp;
	int8_t latHp;
	int8_t heightHp;
	int8_t hMSLHp;
	uint32_t hAcc;
	uint32_t vAcc;
	uint8_t CK_A;
	uint8_t CK_B;
} HPPOSLLH;

// struct containing a full second of UBX packets, one of each type
typedef struct {
	// the string that will be stored in the CSV file
	char CSVString[255];

	// storage for individual packets
	PVT pvt;
	HPPOSLLH hpposllh;

	// has this packet been written to SD card?
	bool written;
} dataLine;

// buffer for actually storing the data
dataLine UBXBuf[BUF_SIZE];
unsigned int head = 0;
unsigned int tail = 0;
unsigned int str_tail = 0;

// the simulated screen buffer
char firstLine[17]; 
char secondLine[17]; 

// global variables for use by the screen UI
bool UI_baseLocked;
int UI_warmupCounter;
int UI_baseLat; int UI_baseLon;
bool UI_svinRunning;
int UI_svinProgress;
int UI_svinEndpoint;
bool UI_roverComm;
bool UI_baseTransmitting;
int UI_roverRTK;
int UI_roverHAcc;

unsigned char buttonState = 0;

#define WARMUP_TIME 10

// defines for button masking
#define UP      0b00001
#define DOWN    0b00010
#define LEFT    0b00100
#define RIGHT   0b01000
#define SELECT  0b10000

// defines for the state machine
#define UI_BOOTUP 0
#define UI_WARMUP 1
#define UI_SVTEXT 2
#define UI_SVSETTINGS 3
#define UI_SVRUNNING 4
#define UI_LOCKED 5
#define UI_NORMAL 6

void printTimestamp();
void fillSlot(int i);

// test harness for the UI
int state;

void loadGlobals() {
	UI_baseLocked = true;
	// CASTING TODO
	UI_baseLat = UBXBuf[0].pvt.lat;
	UI_baseLon = UBXBuf[0].pvt.lon;
	UI_svinRunning = false;
	UI_svinProgress = 0;
	UI_svinEndpoint = 60;
	UI_baseTransmitting = true;
	UI_roverComm = true;
	UI_roverRTK = 0;
	UI_roverHAcc = 300;
}

void runSimulator() {
	static int ctr = 0;
	if(state == UI_BOOTUP || state == UI_WARMUP) {
		buttonState = 0;
	}
	else if (state == UI_SVTEXT) {
		if (ctr < 5) {
			buttonState |= SELECT;
			ctr = 0;
		} else {
			ctr++;
		}
	} 
	else if (state == UI_SVSETTINGS) {
		if (ctr == 0) {
			buttonState = 0;
			ctr++;
		}
		else if (ctr < 5) {
			buttonState |= UP;
			ctr++;
		}
		else if (ctr < 7) {
			buttonState = 0;
			buttonState |= DOWN;
			ctr++;
		} else {
			ctr = 0;
			buttonState = 0;
			buttonState |= RIGHT;
		}
	} else {
		buttonState = 0;
	}
}

// UI functions
void startWarmup();
void continueWarmup();
void svinReady();
void svinSettings();
void svinRun();
void locationLocked();
void baseStatus();
void roverStatus();
void updateDisplay();
void pollButtons();


static void doSum(void * data_in, uint8_t * CK_A, uint8_t * CK_B) {
	uint8_t * buf = (uint8_t *) data_in;
	int len = buf[2] + (buf[3]<<8);

	for (int i = 0; i < len+4; i++) {
		*CK_A += buf[i];
		*CK_B += *CK_A;
	}
}

void startSurvey(uint32_t dur, uint32_t acc) {
	uint8_t buf[38];

	// magic number and CFG-VALSET values
	buf[0] = 0xB5;
	buf[1] = 0x62;
	buf[2] = 0x06;
	buf[3] = 0x8a;
	
	// length (30 bytes currently)
	buf[4] = 30;
	buf[5] = 0;
	
	// layer (xx1 ram, x1x bbm, 1xx flash)
	buf[7] = 0b001;
	
	// reserved fields
	buf[6] = 0; buf[8] = 0; buf[9] = 0;
	
	int idx = 10;
	// start copying in CFG IDs
	// NAVSPG-DYNMODE
	uint32_t cfgID = 0x20110021;
	memcpy(&(buf[idx]), &cfgID, 4); idx += 4;
	buf[idx++] = 2; // STATIC

	// TMODE-SVIN_MIN_DUR 
	cfgID = 0x40030010;
	memcpy(&(buf[idx]), &cfgID, 4); idx += 4;
	memcpy(&(buf[idx]), &dur, 4); idx += 4;

	// TMODE-SVIN_ACC_LIMIT 
	cfgID = 0x40030011;
	memcpy(&(buf[idx]), &cfgID, 4); idx += 4;
	memcpy(&(buf[idx]), &acc, 4); idx += 4;

	// TMODE-MODE 
	cfgID = 0x20030001;
	memcpy(&(buf[idx]), &cfgID, 4); idx += 4;
	buf[idx++] = 1; // SURVEY_IN

	uint8_t CK_A, CK_B;
	doSum(buf+2, &(CK_A), &CK_B);
	buf[idx++] = CK_A;
	buf[idx] = CK_B;

}

// loads up one slot of the UBXBuf with data
int main(int argc, char *argv[]) {
	firstLine[16]='\0';
	secondLine[16]='\0';

	fillSlot(0);

	loadGlobals();

	startSurvey(60, 1000);

	state = UI_BOOTUP;
	bool breakNow = false;

	while(!breakNow) {
		pollButtons();

		switch (state) {

			case UI_BOOTUP:
				startWarmup();
				state = UI_WARMUP;
				break;

			case UI_WARMUP:
				continueWarmup();
				if (UI_warmupCounter == 0) state = UI_SVTEXT;
				break;

			case UI_SVTEXT:
				if (buttonState & SELECT) {
					state = UI_SVSETTINGS;
					break;
				} else {
					svinReady();
					break;
				}
			case UI_SVSETTINGS:
				if (buttonState & RIGHT) {
					UI_svinRunning = true;
					state = UI_SVRUNNING;
					break;
				} else if (buttonState & LEFT) {
					state = UI_SVTEXT;
					break;
				} else {
					if (buttonState & UP && UI_svinEndpoint < 990) {
						UI_svinEndpoint += 15;
					} else if (buttonState & DOWN && UI_svinEndpoint > 0) {
						UI_svinEndpoint -= 15;
					}
					svinSettings();
				}
				break;

			case UI_SVRUNNING:
				// "read" svinProgress
				UI_svinProgress++;

				// if we're done, show the lock
				// this should check packets, not just the counter
				if (UI_svinProgress > UI_svinEndpoint) {
					state = UI_LOCKED;
					break;
				// otherwise, keep running
				} else {
					svinRun();
					break;
				}

			case UI_LOCKED: 
				locationLocked();
				state = UI_NORMAL;
				break;

			case UI_NORMAL:
				baseStatus();
				roverStatus();
				breakNow = true;
				break;

			default:
				break;
		}

		updateDisplay();
	}
}

void printTimestamp() {
	char stampStr[21];
	stampStr[20] = 0;

	// go get the timestamp
	char * startPtr = strchr(UBXBuf[0].CSVString, ',');
	strncpy(stampStr, startPtr+1, 19);

	// make an MS-DOS/FAT compatible filename (MMDDHHMM.CSV)
	stampStr[0] = stampStr[5]; stampStr[1] = stampStr[6]; // month
	stampStr[2] = stampStr[8]; stampStr[3] = stampStr[9]; // day
	stampStr[4] = stampStr[11]; stampStr[5] = stampStr[12]; // hour
	stampStr[6] = stampStr[14]; stampStr[7] = stampStr[15]; // minute
	stampStr[8]='.'; stampStr[9]='C'; stampStr[10]='S'; stampStr[11]='V'; stampStr[12]=0;

	printf("%s\n", stampStr);
}

// fills slot i of the UBXBuf with standard data
void fillSlot(int i) {
	UBXBuf[i].pvt.class = 1;
	UBXBuf[i].pvt.ID = 7;
	UBXBuf[i].pvt.length = 92;
	UBXBuf[i].pvt.iTOW = 318750000;
	UBXBuf[i].pvt.year = 2020;
	UBXBuf[i].pvt.month = 6;
	UBXBuf[i].pvt.day = 24;
	UBXBuf[i].pvt.hour = 16;
	UBXBuf[i].pvt.min = 32;
	UBXBuf[i].pvt.sec = 12;
	UBXBuf[i].pvt.valid = 55; //flags
	UBXBuf[i].pvt.tAcc = 4;
	UBXBuf[i].pvt.nano = 424544;
	UBXBuf[i].pvt.fixType = 3;
	UBXBuf[i].pvt.flags = 1;
	UBXBuf[i].pvt.flags2 = 234;
	UBXBuf[i].pvt.numSV = 18;
	UBXBuf[i].pvt.lon = -729275655;
	UBXBuf[i].pvt.lat = 413190109;
	UBXBuf[i].pvt.height = -10336;
	UBXBuf[i].pvt.hMSL = 23763;
	UBXBuf[i].pvt.hAcc = 998;
	UBXBuf[i].pvt.vAcc = 1795;
	UBXBuf[i].pvt.velN = 5;
	UBXBuf[i].pvt.velE = -4;
	UBXBuf[i].pvt.velD = 14;
	UBXBuf[i].pvt.gSpeed = 6;
	UBXBuf[i].pvt.headMot = 8975177;
	UBXBuf[i].pvt.sAcc = 177;
	UBXBuf[i].pvt.headAcc = 3647734;
	UBXBuf[i].pvt.pDOP = 120;
	UBXBuf[i].pvt.flags3 = 0;
	UBXBuf[i].pvt.headVeh = 0;
	UBXBuf[i].pvt.magDec = 0;
	UBXBuf[i].pvt.macAcc = 0;
	UBXBuf[i].pvt.CK_A = 142;
	UBXBuf[i].pvt.CK_B = 0;


	UBXBuf[i].hpposllh.class = 1;
	UBXBuf[i].hpposllh.ID = 20;
	UBXBuf[i].hpposllh.length = 36;
	UBXBuf[i].hpposllh.version = 0;
	UBXBuf[i].hpposllh.flags = 0;
	UBXBuf[i].hpposllh.iTOW = 318750000;
	UBXBuf[i].hpposllh.lon = -729275655;
	UBXBuf[i].hpposllh.lat = 413190109;
	UBXBuf[i].hpposllh.height = -10336;
	UBXBuf[i].hpposllh.hMSL = 23763;
	UBXBuf[i].hpposllh.lonHp = -24;
	UBXBuf[i].hpposllh.latHp = 17;
	UBXBuf[i].hpposllh.heightHp = -3;
	UBXBuf[i].hpposllh.hMSLHp = 0;
	UBXBuf[i].hpposllh.hAcc = 9976;
	UBXBuf[i].hpposllh.vAcc = 17946;
	UBXBuf[i].hpposllh.CK_A = 34;
	UBXBuf[i].hpposllh.CK_B = 105;
}


// starts the ~1 minute warmup timer
void startWarmup() {
	// displays the old lock or NOT LOCKED on the first line
	if (UI_baseLocked) {
		int lat; int lon;
		firstLine[0] = '#';
		firstLine[1] = ':';
		if (UI_baseLat < 0) { firstLine[2] = '-'; lat = UI_baseLat * -1; }
		else { firstLine[2] = ' '; lat = UI_baseLat; }
		firstLine[3] = (char)(lat/1000000000 + 0x30);
		firstLine[4] = (char)(lat/100000000 - 10*(lat/1000000000) + 0x30);
		firstLine[5] = (char)(lat/10000000 - 10*(lat/100000000) + 0x30);
		firstLine[6] = '.';
		firstLine[7] = (char)(lat/1000000 - 10*(lat/10000000) + 0x30);
		firstLine[8] = ',';
		if (UI_baseLon < 0) { firstLine[9] = '-'; lon = UI_baseLon * -1; }
		else { firstLine[9] = ' '; lon = UI_baseLon; }
		firstLine[10] = (char)(lon/1000000000 + 0x30);
		firstLine[11] = (char)(lon/100000000 - 10*(lon/1000000000) + 0x30);
		firstLine[12] = (char)(lon/10000000 - 10*(lon/100000000) + 0x30);
		firstLine[13] = '.';
		firstLine[14] = (char)(lat/1000000 - 10*(lat/10000000) + 0x30);
	}
	else {
		strncpy(firstLine, "---NOT LOCKED---", 17);
	}

	// starts a warmup counter at 0 on the second line
	strncpy(secondLine, "WARMUP:   s     ", 17);
	secondLine[8] = (char)(WARMUP_TIME/10 + 0x30);
	secondLine[9] = (char)(WARMUP_TIME%10 + 0x30);	
	UI_warmupCounter = WARMUP_TIME;

}

// ticks the warmup timer
void continueWarmup() {
	// decrements the warmup counter
	if (UI_warmupCounter-- > 0) {
		secondLine[8] = (char)(UI_warmupCounter/10 + 0x30);
		secondLine[9] = (char)(UI_warmupCounter%10 + 0x30);
	} else {
		strncpy(secondLine + 8, "COMPLETE", 9);
	}
}

// static screen to explain SVIN process
void svinReady() {
	// displays "SVIN: PRESS SEL\OR USE U-CENTER"
	strncpy(firstLine, "SVIN: PRESS SEL ", 17);
	strncpy(secondLine,"OR USE U-CENTER ", 17);
}

// UI to adjust SVIN settings
void svinSettings() {
	// runs the SVIN UI, handles button presses, etc, accessing local state vars
	firstLine[0] = (char)(UI_svinEndpoint/100 + 0x30);
	firstLine[1] = (char)(UI_svinEndpoint/10 - 10*(UI_svinEndpoint/100) + 0x30);
	firstLine[2] = (char)(UI_svinEndpoint%10 + 0x30);
	strncpy(firstLine+3, "s U/D ADJUST ", 14);
	strncpy(secondLine, "L:BACK RIGHT:RUN", 17);
}

// shows progress on the survey-in
void svinRun() {
	// updates the current value of the SVIN progress bar
	// needs data from the base buffer
	strncpy(firstLine, "SVIN IS RUNNING!", 17);
	if (UI_svinProgress <= UI_svinEndpoint) {
		strncpy(secondLine, "SSS/SSS WAITING ", 17);
		secondLine[0] = (char)(UI_svinProgress/100 + 0x30);
		secondLine[1] = (char)(UI_svinProgress/10 - 10*(UI_svinProgress/100) + 0x30);
		secondLine[2] = (char)(UI_svinProgress%10 + 0x30);
		secondLine[4] = (char)(UI_svinEndpoint/100 + 0x30);
		secondLine[5] = (char)(UI_svinEndpoint/10 - 10*(UI_svinEndpoint/100) + 0x30);
		secondLine[6] = (char)(UI_svinEndpoint%10 + 0x30);
	} else {
		strncpy(secondLine, "WAIT FOR STDDEV ", 17);
	}

}

// fires once when the location is locked
void locationLocked() {
	int lat; int lon;
	firstLine[0] = '#';
	if (UI_baseLat < 0) {
		firstLine[1] = '-';
		lat = UI_baseLat * -1;
	} else { 
		firstLine[1] = ' ';
		lat = UI_baseLat;
	}
	firstLine[2] = (char)(lat/1000000000 + 0x30);
	firstLine[3] = (char)(lat/100000000 - 10*(lat/1000000000) + 0x30);
	firstLine[4] = (char)(lat/10000000 - 10*(lat/100000000) + 0x30);
	firstLine[5] = '.';
	firstLine[6] = (char)(lat/1000000 - 10*(lat/10000000) + 0x30);
	firstLine[7] = (char)(lat/100000 - 10*(lat/1000000) + 0x30);
	firstLine[8] = (char)(lat/10000 - 10*(lat/100000) + 0x30);
	firstLine[9] = (char)(lat/1000 - 10*(lat/10000) + 0x30);
	firstLine[10] = (char)(lat/100 - 10*(lat/1000) + 0x30);
	firstLine[11] = (char)(lat/10 - 10*(lat/100) + 0x30);
	firstLine[12] = (char)(lat - 10*(lat/10) + 0x30);
	firstLine[13] = ' '; firstLine[14] = ' '; firstLine[15] = ' ';

	secondLine[0] = '#';
	if (UI_baseLon < 0) {
		secondLine[1] = '-';
		lon = UI_baseLon * -1;
	} else { 
		secondLine[1] = ' ';
		lon = UI_baseLon;
	}
	secondLine[2] = (char)(lon/1000000000 + 0x30);
	secondLine[3] = (char)(lon/100000000 - 10*(lon/1000000000) + 0x30);
	secondLine[4] = (char)(lon/10000000 - 10*(lon/100000000) + 0x30);
	secondLine[5] = '.';
	secondLine[6] = (char)(lon/1000000 - 10*(lon/10000000) + 0x30);
	secondLine[7] = (char)(lon/100000 - 10*(lon/1000000) + 0x30);
	secondLine[8] = (char)(lon/10000 - 10*(lon/100000) + 0x30);
	secondLine[9] = (char)(lon/1000 - 10*(lon/10000) + 0x30);
	secondLine[10] = (char)(lon/100 - 10*(lon/1000) + 0x30);
	secondLine[11] = (char)(lon/10 - 10*(lon/100) + 0x30);
	secondLine[12] = (char)(lon - 10*(lon/10) + 0x30);
	secondLine[13] = ' '; secondLine[14] = ' '; secondLine[15] = ' ';
	// fills both lines with lat/lon data
	// backlight turns green
}

// updates the line for the base station status
void baseStatus() {
	// warnings for no lock and not transmitting
	// otherwise "LOCK:-LAT,-LON"
	if (!UI_baseLocked) {
		strncpy(firstLine, "!!!NOT LOCKED!!!", 17);
	} else if (!UI_baseTransmitting) {
		strncpy(firstLine, "NOT TRANSMITTING", 17);
	} else {
		int lat; int lon;
		strncpy(firstLine, "LOCK:",5);
		if (UI_baseLat < 0) { firstLine[5] = '-'; lat = UI_baseLat * -1; }
		else { firstLine[5] = ' '; lat = UI_baseLat; }
		firstLine[6] = (char)(lat/1000000000 + 0x30);
		firstLine[7] = (char)(lat/100000000 - 10*(lat/1000000000) + 0x30);
		firstLine[8] = (char)(lat/10000000 - 10*(lat/100000000) + 0x30);
		firstLine[9] = ',';
		if (UI_baseLon < 0) { firstLine[10] = '-'; lon = UI_baseLon * -1; }
		else { firstLine[10] = ' '; lon = UI_baseLon; }
		firstLine[11] = (char)(lon/1000000000 + 0x30);
		firstLine[12] = (char)(lon/100000000 - 10*(lon/1000000000) + 0x30);
		firstLine[13] = (char)(lon/10000000 - 10*(lon/100000000) + 0x30);
		firstLine[14] = ' '; firstLine[15] = ' ';
	}
}

// updates the line for the rover status
void roverStatus() {
	// "[COMM] FIXTY XXXcm"
	if (UI_roverComm) {
		secondLine[0] = '+';
	} else {
		secondLine[0] = 'X';
	}

	secondLine[1] = ' ';

	if (UI_roverRTK == 0) {
		strncpy(secondLine+2, "NORTK", 5);
	} else if (UI_roverRTK == 1) {
		strncpy(secondLine+2, "FLOAT", 5);
	} else if (UI_roverRTK == 2) {
		strncpy(secondLine+2, "FIXED", 5);
	} else {
		strncpy(secondLine+2, "ERROR", 5);
	}

	secondLine[7] = ' ';
	secondLine[8] = (char)(UI_roverHAcc/1000 - 10*(UI_roverHAcc/10000) + 0x30);
	secondLine[9] = (char)(UI_roverHAcc/100 - 10*(UI_roverHAcc/1000) + 0x30);
	secondLine[10] = (char)(UI_roverHAcc/10 - 10*(UI_roverHAcc/100) + 0x30);
	secondLine[11] = '.';
	secondLine[12] = (char)(UI_roverHAcc - 10*(UI_roverHAcc/10) + 0x30);
	secondLine[13] = 'c';
	secondLine[14] = 'm';
	secondLine[15] = ' ';

}

// pushes the changes out to the display
void updateDisplay() {
	printf("%s\n", firstLine);
	printf("%s\n", secondLine);
	printf("\n");
}

// checks the status of the buttons (I hope they're debounced :))))) 
void pollButtons() {
	runSimulator();
	return;
}