######################################
#           Annie Polish             #
#           2019 - 2020              #
#  Drone data processing functions   #
######################################

# REFACTORED!!!
# New and ~~classy~~

import matplotlib as mpl
import matplotlib.cm
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from mpl_toolkits.mplot3d import Axes3D 
import numpy as np
import pandas as pd
import pymap3d as pm
from scipy.stats import norm
from math import *
from datetime import datetime
from matplotlib.transforms import offset_copy
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import geopy.distance
from scalebar import scale_bar #requires the existance of scalebar.py

# globals for plotting
MAPBOX_KEY = "pk.eyJ1IjoiYW5uaWVwb2xpc2giLCJhIjoiY2p5b3BwdXl3MTdhdzNjdDRjbGw5MWJ6ciJ9.01NjskBuc2SQcm5QjbyLwA"
MAPBOX_STYLE = "cjypy1k7x0ru71cjva7cs5iwz"
MAPBOX_USERNAME = "anniepolish"
PLOT_BG = cimgt.MapboxStyleTiles(MAPBOX_KEY, MAPBOX_USERNAME, MAPBOX_STYLE)
BORDER = 0.00003


@pd.api.extensions.register_dataframe_accessor("drone")
class DroneAPI:
    def __init__(self, pandas_obj):
        #print("in init")
        self._validate(pandas_obj)
        self._obj = pandas_obj
        self.name = ""
        #if "timestamp" not in self._obj.columns: self._obj = self._interpolate_time()
        self._obj = self._interpolate_time()

    # Properties
    
    @property
    def center(self, mode="latlon"): # mode may be latlon, azel, or enu
        # return the geographic center point of this DataFrame
        # in lat, lon, hmsl (by default)
        if mode.lower() == "latlon":
            lat = self._obj.Lat
            lon = self._obj.Lon
            hmsl = self._obj.hmsl
            return (float(lat.mean()), float(lon.mean()), float(hmsl.mean()))
        # in az, el, range
        elif mode.lower() == "azel":
            if "az" not in self._obj.columns: 
                print("Error: Local coordinates not created - run df.drone.make_local")
                return -1
            az = self._obj.az
            el = self._obj.el
            ran = self._obj.ran
            return(float(az.mean()), float(el.mean()), float(ran.mean()))
        # in east, north, up
        elif mode.lower() == "enu":
            if "east" not in self._obj.columns:
                print("Error: Local coordinates not created - run df.drone.make_local")
                return -1
            east = self._obj.east
            north = self._obj.north
            up = self._obj.up
            return(float(east.mean()), float(north.mean()), float(up.mean()))
        else:
            print("Error: invalid type. Valid types are LatLon, AzEl, and ENU (case insensitive)")
            return -1


    # Private methods

    @staticmethod
    # validator to check that we have a valid drone file
    def _validate(obj):
        # verify there is a column latitude and a column longitude
        if 'Lat' not in obj.columns or 'Lon' not in obj.columns:
            raise AttributeError("Must have 'Lat' and 'Lon'.")

    # fills in millisecond-scale time stamps in the 'timestamp' column
    # runs on every init
    def _interpolate_time(self):
        df = self._obj
        df = df.assign(datetimestamp = pd.to_datetime(df.datetimestamp))

        #if 'timestamp' in df.columns:
        #    print("Already interpolated")
        #    return df

        # rewritten timestamp interpolation
        # removes dependance on sec_elapsed
        # note that this assumes that the clock ticks (i.e., seconds = **.000) AT the row where that seconds value first appears
        # this may introduce errors of up to 1 row interval (~10ms), and could probably be helped by some interpolation

        last_time = df["datetimestamp"].values[0]
        this_time = df["datetimestamp"].values[1]
        tick_indices = [0] # indices into the df where the datetimestamp ticks over
        tick_sec_elapsed = [] # the value of sec_elapsed when the clock ticks over

        # find all the rows where the datetimestamp ticks over
        for i in range(1, len(df)):
            this_time = df["datetimestamp"].values[i]
            if last_time != this_time: 
                tick_indices.append(i)
                tick_sec_elapsed.append(df["sec_elapsed"].values[i])
            last_time = this_time
        
        tick_indices.append(len(df))
        
        corrected_timestamps = []

        # add in the sub-second values for every second

        second_length = tick_indices[0]

        for i in range (0, len(tick_indices)-1): # for each second
            second_length = tick_indices[i+1]-tick_indices[i]
            for j in range(tick_indices[i+1]-tick_indices[i]): # for each row in a given second
                uncorrected = df["datetimestamp"].values[tick_indices[i]+j] # datetimestamp to correct
                correction = j*(1./second_length) # correction, in seconds
                correction = np.timedelta64(int(correction*1000),'ms') # convert correction to milliseconds
                corrected_timestamps.append(uncorrected + correction) # correct the timestamp

        df["timestamp"] = np.datetime_as_string(corrected_timestamps)
        self._obj["timestamp"] = corrected_timestamps

        return df



    # Public methods
    
    # slices the dataframe using timestamps
    def chop_time(self, startTime, endTime):
        df_slice = self._obj[self._obj["timestamp"] > startTime]
        df_slice = df_slice[df_slice["timestamp"] < endTime]

        df_slice.drone.name = self.name
        return df_slice

    # adds local coordinate columns
    def make_local(self, startTime = 0, endTime = 0, lat_origin=0, lon_origin=0, height_origin=0):
        """ 
        Moves the DataFrame into a local coordinate system

        Requires an origin point of the coordinate system.

        The origin may be specified as either Lat, Lon, Hmsl,
        or as a start and end time.

        This adds the following columns:
        * east : float, UNIT TBD
        * north : float, UNIT TBD
        * up : float, UNIT TBD
        * az : float, degrees
        * el : float, degrees
        * ran : float, UNIT TBD


        Parameters
        ----------
        startTime : np.datetime64, optional
            Beginning of averaging interval for determining the origin
        endTime : np.datetime64, optional
            End of averaging interval for determining the origin
        lat_origin : float, optional
            Latitude of the origin point
        lon_origin : float, optional
            Longitude of the origin point
        height_origin : float, optional
            Height above MSL of the origin point

        Returns
        -------
        pd.DataFrame
            The DataFrame you passed in, with some extra columns 


        """
        if (startTime==0 or endTime==0) and (lat_origin==0 or lon_origin==0):
            print("Error: not enough arguments to specify origin")
            print("Use either start time and end time, or lat, lon, and optional hmsl")
            return self._obj

        df = self._obj

        # use the endpoints of the file as fallback start/end times
        if(startTime == 0): startTime = df['timestamp'].values[0]
        if(endTime == 0): endTime = df['timestamp'].values[-1]

        # find the origin of our transformed coordinate system
        # for z, if the value is not specified, make a reasonable guess
        if (height_origin == 0):
            # try averaging over the interval t_min to t=0
            height_origin = np.mean(df[df["sec_elapsed"] < 0]["hmsl"].values)
            if (height_origin == 0 or np.isnan(height_origin)):
                # if t is always > 0, just pick the lowest height in the dataframe
                height_origin = np.min(df['hmsl'].values)        
        # now find the lat and lon origin
        df_slice = self.chop_time(startTime, endTime)
        if (lat_origin == 0): lat_origin = np.mean(df_slice["Lat"].values)
        if (lon_origin == 0): lon_origin = np.mean(df_slice["Lon"].values)
   

        az, el, ran = pm.geodetic2aer(df["Lat"].values, df["Lon"].values, df["hmsl"].values,
                           lat_origin, lon_origin, height_origin)

        east, north, up = pm.geodetic2enu(df["Lat"].values, df["Lon"].values, df["hmsl"].values,
                                      lat_origin, lon_origin, height_origin)
        
        df = df.assign(az = az)
        df = df.assign(el = el)
        df = df.assign(ran = ran)
        df = df.assign(east = east)
        df = df.assign(north = north)
        df = df.assign(up = up)

        df.drone.name = self.name
        return df

    # do a bunch of statistics about a time interval
    def stats(self, startTime=0, endTime=0):
        """
        |  Performs various statistics on the given time interval
        |  Converts these statistics from lat/lon to meters

        Parameters
        ----------
        startTime : np.datetime64
            Start of measurement interval
        endTime : np.datetime64
            End of measurement interval

        Returns
        -------
        tuple
            Contains: (lat_distance, lon_distance, hmsl_dev, xy_distance)

        """

        df = self._obj
        # slice off part of the dataframe
        if startTime != 0 and endTime != 0:
            df_slice = df[df["timestamp"] > startTime]
            df_slice = df_slice[df_slice["timestamp"] < endTime]
        else:
            print("Error: interval not specified")
            return -1
        lat_arr  = df_slice["Lat"].values
        lon_arr  = df_slice["Lon"].values
        hmsl_arr = df_slice["hmsl"].values
        # find max and min of each coordinate, and the distance that represents
        max_lat  = np.max(lat_arr)
        min_lat  = np.min(lat_arr)
        max_lon  = np.max(lon_arr)
        min_lon  = np.min(lon_arr)
        lat_dist = geopy.distance.distance((min_lat, lon_arr[0]), (max_lat, lon_arr[0])).m
        lon_dist = geopy.distance.distance((lat_arr[0], min_lon), (lat_arr[0], max_lon)).m
        xy_dist = sqrt(lat_dist**2 + lon_dist**2)
        # find the standard deviation of each coordinate
        lat_dev  = np.std(lat_arr)
        lon_dev  = np.std(lon_arr)
        hmsl_dev = np.std(hmsl_arr)
        # print and return
        print("Latitude standard deviation: "+str(lat_dist)+" meters")
        print("Longitude standard deviation: "+str(lon_dist)+" meters")
        print("Height standard deviation: "+str(hmsl_dev)+" meters")
        print("Diagonal lat-lon distance: "+str(xy_dist)+" meters")
        return (lat_dev, lon_dev, hmsl_dev, xy_dist)

    # measure the distance between two timestamps
    def dist(self, startTime, endTime):
        """
        Reports the distance between two timestamps

        Parameters
        ----------
        df : pd.DataFrame
            The DataFrame to examine
        startTime : np.datetime64
            The time at which you measure your first point
        endTime : np.datetime64
            The time at which you measure your second point

        Returns
        -------
        float
            The distance between the two points, in meters

        """
        df = self._obj
        df_slice = df[df["timestamp"] > startTime]
        df_slice = df_slice[df_slice["timestamp"] < endTime]
        start_lat = df_slice.iloc[0]["Lat"]
        start_lon = df_slice.iloc[0]["Lon"]
        end_lat = df_slice.iloc[-1]["Lat"]
        end_lon = df_slice.iloc[-1]["Lon"]
        dist = geopy.distance.distance((start_lat, start_lon), (end_lat, end_lon)).m
        print("distance: "+str(dist))
        return(dist)

    



####################
###   LOAD_CSV   ###
####################


COL_LIST = ["RTKdata:Lat_P", "RTKdata:Lon_P", "RTKdata:Hmsl_P", "GPS:dateTimeStamp", 
"offsetTime", "Tick#", 'IMU_ATTI(0):velComposite', 'RTKdata:YAW', "RTKdata:pitch", 
"IMU_ATTI(0):roll", "IMU_ATTI(0):pitch", "IMU_ATTI(0):yaw", "IMU_ATTI(0):yaw360",
"IMUEX(0):rtk_Latitude", "IMUEX(0):rtk_Longitude", "IMUEX(0):rtk_Alti",#]
"GPS(0):Lat", "GPS(0):Long"]

# global forward declaration
rtk_p = pd.DataFrame(columns=["Lat", "Lon", "hmsl"])
 
# loads the given columns of a single file
def load_columns(filename, columns=COL_LIST):
    FLY = pd.read_csv("./csv_files/"+filename+".csv",sep=",",header=0,usecols=columns)

    # rename columns, do some initial processing
    rtk_p = pd.DataFrame(columns=["Lat", "Lon", "hmsl"])
    rtk_p["Lat"] = FLY["RTKdata:Lat_P"]
    print(len(rtk_p.Lat))
    rtk_p["Lon"] = FLY["RTKdata:Lon_P"]
    rtk_p["hmsl"] = FLY["RTKdata:Hmsl_P"]
    rtk_p["sec_elapsed"] = FLY["offsetTime"]
    rtk_p["datetimestamp"] = FLY["GPS:dateTimeStamp"]
    rtk_p["tick"] = FLY["Tick#"]
    rtk_p["vel"] = FLY["IMU_ATTI(0):velComposite"]
    rtk_p["yaw_rtk"] = FLY["RTKdata:YAW"]
    rtk_p["pitch_rtk"] = FLY["RTKdata:pitch"]
    rtk_p["pitch"] = FLY["IMU_ATTI(0):pitch"]
    rtk_p["roll"] = FLY["IMU_ATTI(0):roll"]
    rtk_p["yaw"] = FLY["IMU_ATTI(0):yaw"]
    rtk_p["yaw_360"] = FLY["IMU_ATTI(0):yaw360"]
    #rtk_p = rtk_p.where(rtk_p != 0.)
    #rtk_p.dropna(inplace=True)
    rtk_p["IMU_lat"] = FLY["IMUEX(0):rtk_Latitude"]
    rtk_p["IMU_lon"] = FLY["IMUEX(0):rtk_Longitude"] 
    rtk_p["IMU_hmsl"] = FLY["IMUEX(0):rtk_Alti"]
    rtk_p["no_rtk_lat"] = FLY["GPS(0):Lat"]
    print(len(rtk_p.no_rtk_lat))
    rtk_p["no_rtk_lon"] = FLY["GPS(0):Long"]
    if len(rtk_p.columns) == 0:
        print("File "+name+" is empty. YEET")
        return(pd.DataFrame(columns=[]))
    return(rtk_p)

# reads the given columns from a file and returns a pandas dataframe
def process_files(filenames, columns=COL_LIST, extra_col = []):
    """
    Processes raw CSVs into much smaller files.

    Accepts multiple files in chronological order, 
    and produces a single file containing all of the data. 

    This should be run once on each new input file.

    Parameters
    ----------
    filenames : list of str
        |  Filenames to load
        |  If you are using one file, it must be passed as ['file']
        |  Also supports multiple files: ['file1', 'file2', etc]
        |  Files should be listed in chronological order

    columns : list of str, optional
        |  Columns to read
        |  load_csv.py has a sane default list

    Returns
    -------
    pd.DataFrame
        A new DataFrame with your data


    """

    if len(extra_col) != 0:
        for col in extra_col:
            columns.append(col)

    frames = []
    for name in filenames:
        df = load_columns(name, columns)

        # this line is ABSOLUTELY NECESSARY
        # you may not think you need to name your file, BUT YOU DO!!!
        # this line is the first invocation of df.drone.SOMETHING,
        # so it runs the _init_() in the droneAPI
        # DroneAPI's _init_ then performs the timestamp interpolation
        # if you don't name the file, you don't get the timestamp column... sorry :(
        df.drone.name = name
        print(df.columns)
        if(df.empty): print("File "+name+" is empty. YEET")
        else: frames.append(df)
    print("STAPLING...")
    df = pd.DataFrame()
    for frame in frames:
        df = pd.concat([df, frame], ignore_index=True)

    if(len(frames) > 1):
        if((len(frames[0]['tick'].values)+len(frames[1]['tick'].values))==len(df['tick'].values)):
            print("STAPLING SUCCEEDED")
        for i in range(len(filenames) - 1):
            print("GAP # "+str(i))
            print(frames[i]['datetimestamp'].values[-1])
            print(frames[i+1]['datetimestamp'].values[0])
        else: print("ERROR: PLEASE CLEAR STAPLER JAM")

    # build a file name:
    name = ""
    for word in filenames:
        name = name + word
        name = name + "_"
    print(name)
    output_df(df, name)
    return df

# outputs the given dataframe as CSV and NPZ
def output_df(df, name):
    """
    |  Outputs your DataFrame to CSV and NPZ
    |  The CSV file will contain all columns
    |  The NPZ file will contain only timestamp, Lat, Lon, and hmsl

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to convert
    name : String
        The name of the data file, for naming output files

    Notes
    -----
    |  Saves NPZ to ./npz/nameDrone_Coordinates.npz
    |  Saves CSV to ./csv_out/nameprocessed.csv

    """
    # output a few columns of the stapled frame as a NPZ file
    np.savez("./npz/"+name+"Drone_Coordinates.npz",timestamp=df['timestamp'].values,Lat=df['Lat'].values,Lon=df['Lon'].values,hmsl=df['hmsl'].values, yaw=df['yaw'].values, vel=df['vel'].values)
    print("Saved NPZ")

    # output the whole stapled frame as a CSV file
    df.to_csv("./csv_out/"+name+"processed.csv")
    print("Saved CSV")


