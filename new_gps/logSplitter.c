#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define FILENAME "termlog"
#define PKTLEN 42+98

int main(int argc, char *argv[]) {
	FILE *f = fopen(FILENAME, "rb");
	FILE *out = fopen("outfile.txt", "w");
	uint8_t b = 0;
	uint8_t last_b = 0;
	bool synced = false;
	int ctr = 0;

	while(fread(&b, 1, 1, f) != 0) {
		fwrite(&b, 1, 1, out);
		ctr++;
		if (ctr >= 144) {
			unsigned char newline = '\n';
			fwrite(&newline, 1, 1, out);
			ctr = 0;
		}
		//last_b = b;	
		
	}

	return 0;
}