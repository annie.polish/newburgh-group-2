#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>



// maximum length of a UBX payload
#define MAX_UBX          128

// size of the packet buffer
#define BUF_SIZE         4

// the header string for the CSV file
//char head_str[] = "iTOW,UTC,nano,tAcc,ttff,msss,Lon,Lat,Height,HMSL,hAcc,vAcc,pDOP,numSV,velN,velE,velD,gspeed,headmot,sAcc,headAcc,LLHerr,UTCerr,fixType,fixOK,diffCorrected";

// structs for individual packets
typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint32_t iTOW;
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t min;
	uint8_t sec;
	uint8_t valid; //flags
	uint32_t tAcc;
	int32_t nano;
	uint8_t fixType;
	uint8_t flags;
	uint8_t flags2;
	uint8_t numSV;
	int32_t lon;
	int32_t lat;
	int32_t height;
	int32_t hMSL;
	uint32_t hAcc;
	uint32_t vAcc;
	int32_t velN;
	int32_t velE;
	int32_t velD;
	int32_t gSpeed;
	int32_t headMot;
	uint32_t sAcc;
	uint32_t headAcc;
	uint16_t pDOP;
	uint8_t flags3;
	uint8_t reserved[5];
	int32_t headVeh;
	int16_t magDec;
	uint16_t macAcc;
	uint8_t CK_A;
	uint8_t CK_B;
} PVT;

typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint8_t version;
	uint16_t reserved;
	uint8_t flags;
	uint32_t iTOW;
	int32_t lon;
	int32_t lat;
	int32_t height;
	int32_t hMSL;
	int8_t lonHp;
	int8_t latHp;
	int8_t heightHp;
	int8_t hMSLHp;
	uint32_t hAcc;
	uint32_t vAcc;
	uint8_t CK_A;
	uint8_t CK_B;
} HPPOSLLH;

typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint32_t iTOW;
	uint8_t gpsFix;
	uint8_t flags;
	uint8_t fixStat;
	uint8_t flags2;
	uint32_t ttff;
	uint32_t msss;
	uint8_t CK_A;
	uint8_t CK_B;
} STATUS;

// struct containing a full second of UBX packets, one of each type
typedef struct {
	// the string that will be stored in the CSV file
	char CSVString[255];

	// storage for individual packets
	PVT pvt;
	HPPOSLLH hpposllh;
	STATUS status;

	// has this packet been written to SD card?
	bool written;
} dataLine;

// buffer for actually storing the data
dataLine UBXBuf[BUF_SIZE];
unsigned int head = 0;
unsigned int tail = 0;
unsigned int str_tail = 0;

int buildString();
void printTimestamp();
void stringNestTest((char *)* arrIn, uint8_t num);

void testConst(int inval) {
	printf("I'm in testConst, inval is %d\n", inval);
	return;
}

#define TESTCONST 42

void printVal(uint8_t * inval) {
	printf("%c\n", *inval);
}

// loads up one slot of the UBXBuf with data
int main(int argc, char *argv[]) {
	testConst(TESTCONST);

	UBXBuf[0].pvt.class = 1;
	UBXBuf[0].pvt.ID = 7;
	UBXBuf[0].pvt.length = 92;
	UBXBuf[0].pvt.iTOW = 318750000;
	UBXBuf[0].pvt.year = 2020;
	UBXBuf[0].pvt.month = 6;
	UBXBuf[0].pvt.day = 24;
	UBXBuf[0].pvt.hour = 16;
	UBXBuf[0].pvt.min = 32;
	UBXBuf[0].pvt.sec = 12;
	UBXBuf[0].pvt.valid = 55; //flags
	UBXBuf[0].pvt.tAcc = 4;
	UBXBuf[0].pvt.nano = 424544;
	UBXBuf[0].pvt.fixType = 3;
	UBXBuf[0].pvt.flags = 1;
	UBXBuf[0].pvt.flags2 = 234;
	UBXBuf[0].pvt.numSV = 18;
	UBXBuf[0].pvt.lon = -729275655;
	UBXBuf[0].pvt.lat = 413190109;
	UBXBuf[0].pvt.height = -10336;
	UBXBuf[0].pvt.hMSL = 23763;
	UBXBuf[0].pvt.hAcc = 998;
	UBXBuf[0].pvt.vAcc = 1795;
	UBXBuf[0].pvt.velN = 5;
	UBXBuf[0].pvt.velE = -4;
	UBXBuf[0].pvt.velD = 14;
	UBXBuf[0].pvt.gSpeed = 6;
	UBXBuf[0].pvt.headMot = 8975177;
	UBXBuf[0].pvt.sAcc = 177;
	UBXBuf[0].pvt.headAcc = 3647734;
	UBXBuf[0].pvt.pDOP = 120;
	UBXBuf[0].pvt.flags3 = 0;
	UBXBuf[0].pvt.headVeh = 0;
	UBXBuf[0].pvt.magDec = 0;
	UBXBuf[0].pvt.macAcc = 0;
	UBXBuf[0].pvt.CK_A = 142;
	UBXBuf[0].pvt.CK_B = 0;


	UBXBuf[0].hpposllh.class = 1;
	UBXBuf[0].hpposllh.ID = 20;
	UBXBuf[0].hpposllh.length = 36;
	UBXBuf[0].hpposllh.version = 0;
	UBXBuf[0].hpposllh.flags = 0;
	UBXBuf[0].hpposllh.iTOW = 318750000;
	UBXBuf[0].hpposllh.lon = -729275655;
	UBXBuf[0].hpposllh.lat = 413190109;
	UBXBuf[0].hpposllh.height = -10336;
	UBXBuf[0].hpposllh.hMSL = 23763;
	UBXBuf[0].hpposllh.lonHp = -24;
	UBXBuf[0].hpposllh.latHp = 17;
	UBXBuf[0].hpposllh.heightHp = -3;
	UBXBuf[0].hpposllh.hMSLHp = 0;
	UBXBuf[0].hpposllh.hAcc = 9976;
	UBXBuf[0].hpposllh.vAcc = 17946;
	UBXBuf[0].hpposllh.CK_A = 34;
	UBXBuf[0].hpposllh.CK_B = 105;

	printf("%u\n", (uint8_t)UBXBuf[0].hpposllh.lonHp);

	for (int i = 0; i < 255; i++) {
		UBXBuf[0].CSVString[i] = 0;
	}

	buildString();

	//printf("%s\n",UBXBuf[0].CSVString);

	printTimestamp();

	uint8_t one = 65;
	printVal(&one);

	char testArr[3][10] = {"0123456789","abcdefghij","klmnopqrst"};
	stringNestTest(testArr, 3);

}

int buildString() {
	// go get the string we'll be writing to
	char * str = UBXBuf[str_tail].CSVString;
	int idx = 0;

	// start copying values into the string
	
	// GPS time of week
	idx += sprintf(str, "%u,", UBXBuf[str_tail].pvt.iTOW);

	// UTC timestamp string
	int micros = UBXBuf[str_tail].pvt.nano / 1000;
	idx += sprintf(str+idx, "%u-%02u-%02uT%02u:%02u:%02u.%06uZ,", UBXBuf[str_tail].pvt.year, 
		   UBXBuf[str_tail].pvt.month, UBXBuf[str_tail].pvt.day, UBXBuf[str_tail].pvt.hour,
		   UBXBuf[str_tail].pvt.min, UBXBuf[str_tail].pvt.sec, micros);

	// tAcc
	idx += sprintf(str+idx, "%u,", UBXBuf[str_tail].pvt.tAcc);

	// Longitude
	int lonUnits = UBXBuf[str_tail].hpposllh.lon/10000000;
	int lonDec = (UBXBuf[str_tail].hpposllh.lon - lonUnits*10000000) * 100;
	lonDec += UBXBuf[str_tail].hpposllh.lonHp;
	if (lonUnits == 0 && lonDec < 0) { idx+=sprintf(str+idx, "-%d.%09u,",lonUnits,lonDec*-1); }
	else if (lonDec<0) { idx+=sprintf(str+idx, "%d.%09u,",lonUnits,lonDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%09u,",lonUnits,lonDec); }

	// Latitude
	int latUnits = UBXBuf[str_tail].hpposllh.lat/10000000;
	int latDec = (UBXBuf[str_tail].hpposllh.lat - latUnits*10000000) * 100;
	latDec += UBXBuf[str_tail].hpposllh.latHp;
	if (latUnits == 0 && latDec < 0) { idx+=sprintf(str+idx, "-%d.%09u,",latUnits,latDec*-1); }
	else if (latDec<0) { idx+=sprintf(str+idx, "%d.%09u,",latUnits,latDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%09u,",latUnits,latDec); }

	// Height above geoid
	int heightUnits = UBXBuf[str_tail].hpposllh.height/1000;
	int heightDec = (UBXBuf[str_tail].hpposllh.height - heightUnits*1000) * 10;
	heightDec += UBXBuf[str_tail].hpposllh.heightHp;
	if (heightUnits == 0 && heightDec < 0) { idx+=sprintf(str+idx, "-%d.%04u,",heightUnits,heightDec*-1); }
	else if (heightDec<0) { idx+=sprintf(str+idx, "%d.%04u,",heightUnits,heightDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%04u,",heightUnits,heightDec); }

	// Height above mean sea level
	int hmslUnits = UBXBuf[str_tail].hpposllh.hMSL/1000;
	int hmslDec = (UBXBuf[str_tail].hpposllh.hMSL - hmslUnits*1000) * 10;
	hmslDec += UBXBuf[str_tail].hpposllh.hMSLHp;
	if (hmslUnits == 0 && hmslDec < 0) { idx+=sprintf(str+idx, "-%d.%04u,",hmslUnits,hmslDec*-1); }
	else if (hmslDec<0) { idx+=sprintf(str+idx, "%d.%04u,",hmslUnits,hmslDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%04u,",hmslUnits,hmslDec); }

	// accuracy, dilution of presicsion, and number of satellites
	idx += sprintf(str+idx, "%u,%u,%3u,%u,", UBXBuf[str_tail].hpposllh.hAcc, UBXBuf[str_tail].hpposllh.vAcc,
		UBXBuf[str_tail].pvt.pDOP, UBXBuf[str_tail].pvt.numSV);

	// North velocity
	int nUnits = UBXBuf[str_tail].pvt.velN/1000;
	int nDec = UBXBuf[str_tail].pvt.velN - nUnits*1000;
	if (nUnits == 0 && nDec < 0) { idx+=sprintf(str+idx, "-%d.%03u,",nUnits,nDec*-1); }
	else if (nDec<0) { idx+=sprintf(str+idx, "%d.%03u,",nUnits,nDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%03u,",nUnits,nDec); }

	// East velocity
	int eUnits = UBXBuf[str_tail].pvt.velE/1000;
	int eDec = UBXBuf[str_tail].pvt.velE - eUnits*1000;
	if (eUnits == 0 && eDec < 0) { idx+=sprintf(str+idx, "-%d.%03u,",eUnits,eDec*-1); }
	else if (eDec<0) { idx+=sprintf(str+idx, "%d.%03u,",eUnits,eDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%03u,",eUnits,eDec); }

	// Down velocity
	int dUnits = UBXBuf[str_tail].pvt.velD/1000;
	int dDec = UBXBuf[str_tail].pvt.velD - dUnits*1000;
	if (dUnits == 0 && dDec < 0) { idx+=sprintf(str+idx, "-%d.%03u,",dUnits,dDec*-1); }
	else if (dDec<0) { idx+=sprintf(str+idx, "%d.%03u,",dUnits,dDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%03u,",dUnits,dDec); }

	// Ground speed
	int sUnits = UBXBuf[str_tail].pvt.gSpeed/1000;
	int sDec = UBXBuf[str_tail].pvt.gSpeed - sUnits*1000;
	if (sUnits == 0 && sDec < 0) { idx+=sprintf(str+idx, "-%d.%03u,",sUnits,sDec*-1); }
	else if (sDec<0) { idx+=sprintf(str+idx, "%d.%03u,",sUnits,sDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%03u,",sUnits,sDec); }

	// Ground speed accuracy
	int saccUnits = UBXBuf[str_tail].pvt.sAcc/1000;
	int saccDec = UBXBuf[str_tail].pvt.sAcc - saccUnits*1000;
	idx+=sprintf(str+idx, "%d.%03u,",saccUnits,saccDec);

	// Heading
	int headUnits = UBXBuf[str_tail].pvt.headMot/100000;
	int headDec = UBXBuf[str_tail].pvt.headMot - headUnits*100000;
	if (headUnits == 0 && headDec < 0) { idx+=sprintf(str+idx, "-%d.%05u,",headUnits,headDec*-1); }
	else if (headDec<0) { idx+=sprintf(str+idx, "%d.%05u,",headUnits,headDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%05u,",headUnits,headDec); }

	// Heading accuracy	
	int haccUnits = UBXBuf[str_tail].pvt.headAcc/100000;
	int haccDec = UBXBuf[str_tail].pvt.headAcc - haccUnits*100000;
	idx+=sprintf(str+idx, "%d.%05u,",haccUnits,haccDec);

	// errors and flags
	int hppos_valid = !(UBXBuf[str_tail].hpposllh.flags & 1);
	int utc_flags = UBXBuf[str_tail].pvt.flags2;
	int utc_valid = (utc_flags & 1<<7) && (utc_flags & 1<<6) && (utc_flags & 1<<5);
	int fix_valid = UBXBuf[str_tail].pvt.flags & 1;
	int diff_valid = UBXBuf[str_tail].pvt.flags & 1<<1;
	idx += sprintf(str+idx, "%u,%u,%u,%u,%u,",UBXBuf[str_tail].pvt.fixType,fix_valid,utc_valid,hppos_valid,diff_valid);
}

void printTimestamp() {
	char stampStr[21];
	stampStr[20] = 0;

	// go get the timestamp
	char * startPtr = strchr(UBXBuf[0].CSVString, ',');
	strncpy(stampStr, startPtr+1, 19);

	// make an MS-DOS/FAT compatible filename (MMDDHHMM.CSV)
	stampStr[0] = stampStr[5]; stampStr[1] = stampStr[6]; // month
	stampStr[2] = stampStr[8]; stampStr[3] = stampStr[9]; // day
	stampStr[4] = stampStr[11]; stampStr[5] = stampStr[12]; // hour
	stampStr[6] = stampStr[14]; stampStr[7] = stampStr[15]; // minute
	stampStr[8]='.'; stampStr[9]='C'; stampStr[10]='S'; stampStr[11]='V'; stampStr[12]=0;

	printf("%s\n", stampStr);


	uint8_t toRead = 0;//(ubxTail - 1)%BUF_SIZE;

	// build a UTC timestamp
	char utcStamp[32];

	int micros = UBXBuf[toRead].pvt.nano / 1000;
	sprintf(utcStamp, "%u-%02u-%02uT%02u:%02u:%02u.%06uZ\n", UBXBuf[toRead].pvt.year,
			UBXBuf[toRead].pvt.month, UBXBuf[toRead].pvt.day, UBXBuf[toRead].pvt.hour,
			UBXBuf[toRead].pvt.min, UBXBuf[toRead].pvt.sec, micros);

	sprintf(utcStamp, "%u-%02u-%02uT%02u:%02u:%02uZ\n", UBXBuf[toRead].pvt.year,
		UBXBuf[toRead].pvt.month, UBXBuf[toRead].pvt.day, UBXBuf[toRead].pvt.hour,
		UBXBuf[toRead].pvt.min, UBXBuf[toRead].pvt.sec);

	printf("%s\n", utcStamp);
}

void stringNestTest((char *)* arrIn, uint8_t num) {
	for (int i = 0; i < num; i++) {
		printf("%s\n", arrIn[i]);
	}
}