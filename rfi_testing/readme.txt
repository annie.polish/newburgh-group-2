Annie Polish
Summer 2019

These files are for analyzing data from the spectrum analyzer.
They are adapted from a notebook that Emily gave me.

If you want to use these, make a copy of the notebook in log_per_flight/
Don't reuse the same notebook for multiple RFI tests (trust me, it sucks).
