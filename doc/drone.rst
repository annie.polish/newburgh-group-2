Drone Data Extraction and Preprocessing
=======================================

How to produce the data that these scripts would like to read

Download data from drone
------------------------
1. Turn on the remote control and iPad
2. Turn on the drone
3. In DJI GO, open the right settings menu, tap the top icon of a drone, 
   scroll all the way down, and tap "Enter Flight Data mode"
4. Plug a microUSB cable into the drone and your computer
5. The drone will appear as a USB storage device. 
6. Copy the most recent bunch of DAT files. 
   A single drone flight may produce many DAT files.


Convert from DAT to CSV
-----------------------
To convert from DAT files to CSV files, you will need DatCon. 
It can be downloaded from https://datfile.net/DatCon/downloads.html

Run DatCon however you normally run jar files (java -jar <filename> on linux).

In the top menu bar, select Categories and make sure Basic and Experimental are both checked. 
Also in the top menu, select Parsing Options, and make sure it's set to Engineered Only, 
and that Allow Invalid DatHeader and Allow Excessive Errors are both checked. 
In the main interface of DatCon, set the Sample Rate to 100Hz.

Click in the box that says "Click here to specify .DAT file" and select one of the files you downloaded from the drone. 
Make sure that the .CSV radio button is selected. 
You may also want to check the radio button in the KML section for Ground Track. 
This will produce a file suitable for opening in Google Earth, but using the onboard GPS, not the RTK. 

Now, press the giant button at the bottom that says "GO!". 
A CSV file will appear in the same directory as your DAT files. 
Repeat for each of your DAT files. 


