#!/home/ap2339/anaconda3/bin/python

# Annie Polish, November 2021
# Script to cut uBlox files into chunks that each match a DJI file

import numpy as np
import pandas as pd
import glob

# these should be passed as arguments
ubx_path = "/hirax/all_drone_data/gbo_oct_21/"
dji_path = "/hirax/all_drone_data/gbo_oct_21/dji/"
ubx_out_path = "/hirax/all_drone_data/gbo_oct_21/ublox_split/"

# (11/12/2021) function for adding sub-second accuracy to DJI timestamps
# now detects and skips backwards jumps in the GPS:dateTimeStamp column
def interp_time(df_in):

    # find where the GPS turns on
    gps_idx = df_in[df_in.gpsUsed == True].index[0]

    # detect places where the GPS:dateTimeStamp column jumps backwards by a large amount, and skip them
    ts_dt = pd.to_datetime(df_in["GPS:dateTimeStamp"][gps_idx:])
    if np.argmin(ts_dt) > gps_idx:
        gps_idx += np.argmin(ts_dt)
        print("Skipped a section where time ran backwards")

    # interpolate the time and see if it works out!
    while (gps_idx < len(df_in)):

        # look for where the datetimestamp ticks
        first_dts = df_in["GPS:dateTimeStamp"][gps_idx]
        start_sec = int(first_dts[-3:-1])
        while(int(df_in["GPS:dateTimeStamp"][gps_idx][-3:-1]) == start_sec):
            gps_idx = gps_idx + 1

        # use this reference timestamp to convert the offsetTime column into proper datetimes
        start_dt = pd.to_datetime(df_in["GPS:dateTimeStamp"][gps_idx])
        offsets = np.array(df_in["offsetTime"]-df_in["offsetTime"][gps_idx])
        offsets = pd.to_timedelta(offsets, unit='s')
        timestamps = start_dt + offsets

        # put them in the dataframe
        df_in = df_in.assign(timestamp = timestamps)
        df_in = df_in.assign(UTC = timestamps)

        # check for excessive error by comparing the interpolated and uninterpolated timestamp columns
        gps_dts = pd.to_datetime(df_in["GPS:dateTimeStamp"][gps_idx:-20]).values
        interp_dts = pd.to_datetime(df_in["timestamp"][gps_idx:-20]).values

        if (np.mean(np.abs(gps_dts - interp_dts)/np.timedelta64(1,'ms')) < 1000):
            print("Timestamp interpolation succeeded")
            break
        else:
            print("Detected >1s error, retrying")
            gps_idx += 11 # increment the start timestamp index by an arbitrary amount and retry

    return df_in

# load in all of the uBlox data and staple it together into a single giant time-series
all_ubx_paths=np.sort(glob.glob(ubx_path+"*"))
ubx_df = pd.read_csv(all_ubx_paths[0], sep=',', header=1)
for path in all_ubx_paths[1:]:
    tmp_df = pd.read_csv(path, sep=',', header=1)
    ubx_df = pd.concat([ubx_df, tmp_df], axis=0, join='inner')
    ubx_df.reset_index(drop=True, inplace=True)

# fix up the timestamp column
ubx_df = ubx_df.assign(UTC = pd.to_datetime(ubx_df.UTC, utc=True))
ubx_df = ubx_df.set_index('UTC')

# enumerate all the DJI files to load
all_dji_paths = np.sort(glob.glob(dji_path+"*"))
print(all_dji_paths)

# iterate over all the DJI files
for dji_fname in all_dji_paths:
    if dji_fname[-10:-4] in exclude_list:
        continue
    # load in the data
    df = pd.read_csv(dji_fname, sep=',', header=0)
    df = interp_time(df)
    df = df.assign(UTC = pd.to_datetime(df.timestamp, utc=True))
    df = df.set_index('UTC')
    
    # find the beginning and end of the useful data in the DJI file
    good_ts = df[df["gpsUsed"]==True].index
    if (len(good_ts) > 2):
        first_ts = good_ts[10]
        last_ts = good_ts[-10]

        # use the timestamps to index into the giant ublox df
        date_mask = (ubx_df.index > first_ts) & (ubx_df.index < last_ts)
        ubx_slice = ubx_df[date_mask]

        # sanity check: was the uBlox GPS even running?
        if (len(ubx_slice) < 2):
            print("WARNING: did not find matching uBlox data for "+dji_fname.split('/')[-1].split('.')[0])
            continue

        # check if we got the whole flight
        if((last_ts-first_ts)-(ubx_slice.index[-1]-ubx_slice.index[0]) > np.timedelta64(10, 's')):
            print("WARNING: more than 10 seconds of data missing in the uBlox file")
        else:
            print("Found whole flight in uBlox file")

        # save the uBlox file
        name = dji_fname.split('/')[-1].split('.')[0]+"_ubx.csv"
        ubx_slice.to_csv(ubx_out_path+name)
        print("Saved "+name)
        
    else:
        print("uh oh. no GPS here!")
    
    # explicitly free the df so we don't try to load all the DJI data into ram all at once
    del df

print("Finished splitting files")
