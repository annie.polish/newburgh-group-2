import matplotlib as mpl
import matplotlib.cm
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from mpl_toolkits.mplot3d import Axes3D 
import numpy as np
import pandas as pd
import pymap3d as pm
#from math import *
from datetime import datetime
from matplotlib.transforms import offset_copy
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import geopy.distance
from scalebar import scale_bar #requires the existance of scalebar.py

import drone

plt.rcParams["figure.figsize"] = (5,5)

drone.process_files(['FLY342'])
#drone.process_files(['FLY236'])
#drone.process_files(['FLY204'])
#drone.process_files(['FLY215'])

#drone.process_files(['FLY206', 'FLY207'])
#drone.process_files(['FLY206', 'FLY207', 'FLY208', 'FLY209', 'FLY210'])
