Analysis of absolute position ground tests

Log from Oct 16:
3:17 - drone on, heading N
3:22 - turn 90deg to W
3:27 - turn 90deg to S
3:32 - turn 90deg to E
3:37 - turn 90deg to N
3:42 - drone left on, pointing N, as we did other things
3:50 - beginning of grid tests
3:53 - 1cm E
3:56 - 2cm E
4:00 - 7cm E
4:03 - 1cm N
4:06 - 2cm N
4:09 - 5cm N
4:13 - all power off, heading N, 5cm N of marker
4:15 - all power on, all devices at same locaton
4:17 - all power off
4:19 - all power on, still same location
4:21 - all power off
4:23 - all power on, RTK has moved

3pm -> 2200 UTC


Stability/stddev within a single point
Verify distance and direction of grid tests
Power cycle displacement


