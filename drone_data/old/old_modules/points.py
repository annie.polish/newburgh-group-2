################################
#         Annie Polish         #
#         Summer 2019          #
# Point tagging and statistics #
################################


import pandas as pd
import numpy as np
from datetime import datetime
import geopy.distance
from columns import *

# returns the statistics of the coordinates in a given time interval
def interval_stats(df, startTime, endTime, avg=False):
    """
    |  Performs various statistics on the given time interval
    |  Converts these statistics from lat/lon to meters

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to examine
    startTime : np.datetime64
        Start of measurement interval
    endTime : np.datetime64
        End of measurement interval
    avg : bool, optional
        Use moving average? Defaults to false

    Returns
    -------
    tuple
        Contains: (lat_distance, lon_distance, hmsl_dev, xy_distance)

    """


    print('hello stationary stats')
    # slice off part of the dataframe
    df_slice = df[df["timestamp"] > startTime]
    df_slice = df_slice[df_slice["timestamp"] < endTime]
    if (avg):
        lat_arr = df_slice["avMovLat"].values
        lon_arr = df_slice["avMovLon"].values
        hmsl_arr= df_slice["avMovHmsl"].values
    else:
        lat_arr  = df_slice["Lat"].values
        lon_arr  = df_slice["Lon"].values
        hmsl_arr = df_slice["hmsl"].values
    # find max and min of each coordinate, and the distance that represents
    max_lat  = np.max(lat_arr)
    min_lat  = np.min(lat_arr)
    max_lon  = np.max(lon_arr)
    min_lon  = np.min(lon_arr)
    lat_dist = geopy.distance.distance((min_lat, lon_arr[0]), (max_lat, lon_arr[0])).m
    lon_dist = geopy.distance.distance((lat_arr[0], min_lon), (lat_arr[0], max_lon)).m
    print("Latitude spread: "+str(lat_dist))
    print("Longitude spread: "+str(lon_dist))
    print("Diagonal distance: "+str(sqrt(lat_dist**2 + lon_dist**2)))
    print("Double check: "+str(geopy.distance.distance((min_lat, min_lon),(max_lat, max_lon)).m))
    # find the standard deviation of each coordinate
    lat_dev  = np.std(lat_arr)
    lon_dev  = np.std(lon_arr)
    hmsl_dev = np.std(hmsl_arr)
    # convert stddev in degrees to stddev in meters
    startCoords = (lat_arr[0], lon_arr[0])
    latCoords = (lat_arr[0]+lat_dev, lon_arr[0])
    lonCoords = (lat_arr[0], lon_arr[0]+lon_dev)
    endCoords = (lat_arr[0]+lat_dev, lon_arr[0]+lon_dev)
    lat_dist = geopy.distance.distance(startCoords, latCoords).m
    lon_dist = geopy.distance.distance(startCoords, lonCoords).m
    xy_dist = geopy.distance.distance(startCoords, endCoords).m
    # print and return
    print("Latitude standard deviation: "+str(lat_dist)+" meters")
    print("Longitude standard deviation: "+str(lon_dist)+" meters")
    print("Height standard deviation: "+str(hmsl_dev)+" meters")
    print("Diagonal lat-lon stddev: "+str(xy_dist)+" meters")
    return (lat_dist, lon_dist, hmsl_dev, xy_dist)

# returns the statistics of the coordinates in a given time interval
def point_stats(df, point, avg=False):
    """
    |  Performs various statistics on the given point
    |  Converts these statistics from lat/lon to meters

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to examine
    point : int
        Which point is being examined
    avg : bool, optional
        Use moving average? Defaults to false

    Returns
    -------
    tuple
        Contains: (lat_distance, lon_distance, hmsl_dev, xy_distance)

    """


    print('hello stationary stats')
    # slice off part of the dataframe
    df_slice = df[df["point"] == point]
    if (avg):
        lat_arr = df_slice["avMovLat"].values
        lon_arr = df_slice["avMovLon"].values
        hmsl_arr= df_slice["avMovHmsl"].values
    else:
        lat_arr  = df_slice["Lat"].values
        lon_arr  = df_slice["Lon"].values
        hmsl_arr = df_slice["hmsl"].values
    # find max and min of each coordinate, and the distance that represents
    max_lat  = np.max(lat_arr)
    min_lat  = np.min(lat_arr)
    max_lon  = np.max(lon_arr)
    min_lon  = np.min(lon_arr)
    lat_dist = geopy.distance.distance((min_lat, lon_arr[0]), (max_lat, lon_arr[0])).m
    lon_dist = geopy.distance.distance((lat_arr[0], min_lon), (lat_arr[0], max_lon)).m
    print("Latitude spread: "+str(lat_dist))
    print("Longitude spread: "+str(lon_dist))
    print("Diagonal distance: "+str(sqrt(lat_dist**2 + lon_dist**2)))
    print("Double check: "+str(geopy.distance.distance((min_lat, min_lon),(max_lat, max_lon)).m))
    # find the standard deviation of each coordinate
    lat_dev  = np.std(lat_arr)
    lon_dev  = np.std(lon_arr)
    hmsl_dev = np.std(hmsl_arr)
    # convert stddev in degrees to stddev in meters
    startCoords = (lat_arr[0], lon_arr[0])
    latCoords = (lat_arr[0]+lat_dev, lon_arr[0])
    lonCoords = (lat_arr[0], lon_arr[0]+lon_dev)
    endCoords = (lat_arr[0]+lat_dev, lon_arr[0]+lon_dev)
    lat_dist = geopy.distance.distance(startCoords, latCoords).m
    lon_dist = geopy.distance.distance(startCoords, lonCoords).m
    xy_dist = geopy.distance.distance(startCoords, endCoords).m
    # print and return
    print("Latitude standard deviation: "+str(lat_dist)+" meters")
    print("Longitude standard deviation: "+str(lon_dist)+" meters")
    print("Height standard deviation: "+str(hmsl_dev)+" meters")
    print("Diagonal lat-lon stddev: "+str(xy_dist)+" meters")
    return (lat_dist, lon_dist, hmsl_dev, xy_dist)



# measure the distance between two second markers
def dist_between_times(df, startTime, endTime):
    """
    Reports the distance between two timestamps

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to examine
    startTime : np.datetime64
        The time at which you measure your first point
    endTime : np.datetime64
        The time at which you measure your second point

    Returns
    -------
    float
        The distance between the two points, in meters

    """
    df_slice = df[df["timestamp"] > startTime]
    df_slice = df_slice[df_slice["timestamp"] < endTime]
    start_lat = df_slice.iloc[0]["Lat"]
    start_lon = df_slice.iloc[0]["Lon"]
    end_lat = df_slice.iloc[-1]["Lat"]
    end_lon = df_slice.iloc[-1]["Lon"]
    dist = geopy.distance.distance((start_lat, start_lon), (end_lat, end_lon)).m
    print(start_lat)
    print(start_lon)
    print(end_lat)
    print(end_lon)
    print("distance: "+str(dist))
    return(dist)

# mark a point by flipping values in the point column
def mark_point(df, timestamp, thresh = 0.2):
    """
    Records a point into the DataFrame

    Starts from the given timestamp, and expands the range out. 
    Range is expanded until the coordinates differ by more than the thresh. 
    The whole range then has its 'point' column updated

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to examine and modify
    timestamp : np.datetime64
        The timestamp to use as the origin of the point
    thresh : float, optional
        Threshold bounding a point. Defaults to 0.2. 
        Increase this value to make points more error-tolerant. 
        Decrease it to make points cleaner

    Returns
    -------
    pd.DataFrame
        The DataFrame you passed in, with one more point tagged

    """
    # if the point column does not exist, make it
    if(not "point" in df.columns):
        df = df.assign(point = np.full_like(df['tick'].values, -1))
        
    # select a 10s window around the input point and average it
    est_delta = np.timedelta64(5, 's')
    df_tmp = chop_time(df, timestamp-est_delta, timestamp+est_delta)
    start_lat = np.mean(df_tmp["Lat"].values)
    start_lon = np.mean(df_tmp["Lon"].values)
    start_hmsl= np.mean(df_tmp["hmsl"].values)
    
    # find the starting location
    init_index = 0
    for stamp in df['timestamp'].values:
        if stamp < timestamp:
            init_index += 1
        else: break
    
    # find the left edge of the point
    left_offset = 0
    while(init_index - left_offset > 0):
        ind = init_index - left_offset
        lat = df['Lat'].values[ind]
        lon = df['Lon'].values[ind]
        hmsl= df['hmsl'].values[ind]
        
        dist = geopy.distance.distance((start_lat, start_lon), (lat, lon)).m
        if (dist < thresh): left_offset += 1
        else: break
    left_index = init_index - left_offset
    print(df['timestamp'].values[left_index])
    
    # find the right edge of the point
    right_offset = 0
    while(init_index + right_offset < len(df['tick'].values)-1):
        ind = init_index + right_offset
        lat = df['Lat'].values[ind]
        lon = df['Lon'].values[ind]
        hmsl= df['hmsl'].values[ind]
        
        dist = geopy.distance.distance((start_lat, start_lon), (lat, lon)).m
        if (dist < thresh): right_offset += 1
        else: break
    right_index = init_index + right_offset
    print(df['timestamp'].values[right_index])
    
    points_in = df['point'].values
    this_point = np.max(points_in) + 1
    print(np.max(points_in))
    for i in range(left_index, right_index):
        points_in[i] = this_point
    df = df.assign(point = points_in)

    return(df)

# output the points into an NPZ
def output_points(df, name):
    """
    Outputs a list of points to an NPZ file

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to output
    name : String
        The name of the dataset, for naming the output file

    Notes
    -----
    Saves data to ./npz/name_points.npz

    """
    mean_alt, std_alt, mean_az, std_az = [],[],[],[]
    
    for i in range(max(df['point'].values)):
        az = df[df['point'] == i]['az'].values
        el = df[df['point'] == i]['el'].values
        #ang = np.subtract(90, el)
        #for j in range(len(az)):
        #    if(az[j] > 180):
        #        az[j] = np.subtract(az[j], 180)
        #        ang[j] = np.multiply(-1., ang[j])
        mean_alt.append(np.mean(el))
        mean_az.append(np.mean(az))
        std_alt.append(np.std(el))
        std_az.append(np.std(az))

    np.savez("./npz/"+name+"_points.npz", mean_alt=mean_alt, std_alt=std_alt, mean_az=mean_az, std_az=std_az)
    print("Saved NPZ")











