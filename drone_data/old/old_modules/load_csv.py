#########################
#     Annie Polish      #
#     Summer 2019       #
# CSV parsing functions #
#########################

# README: How to prepare data for this script
# Open DatCon (from datfile.net)
# Set Parsing Options to "Engineered Only"
# Set the Sample Rate to 100Hz
# Set the output location to ./CSV_files (relative to this script)
# GO!


import numpy as np
import pandas as pd
from math import *
from datetime import datetime

COL_LIST = ["RTKdata:Lat_P", "RTKdata:Lon_P", "RTKdata:Hmsl_P", "GPS:dateTimeStamp", "offsetTime", "Tick#"]

# global forward declaration
rtk_p = pd.DataFrame(columns=["Lat", "Lon", "hmsl"])
 
# loads the given columns of a single file
def load_columns(filename, columns=COL_LIST):
    FLY = pd.read_csv("./csv_files/"+filename+".csv",sep=",",header=0,usecols=columns)

    # rename columns, do some initial processing
    rtk_p = pd.DataFrame(columns=["Lat", "Lon", "hmsl"])
    rtk_p["Lat"] = FLY["RTKdata:Lat_P"]
    rtk_p["Lon"] = FLY["RTKdata:Lon_P"]
    rtk_p["hmsl"] = FLY["RTKdata:Hmsl_P"]
    rtk_p["sec_elapsed"] = FLY["offsetTime"]
    rtk_p["datetimestamp"] = FLY["GPS:dateTimeStamp"]
    rtk_p["tick"] = FLY["Tick#"]
    rtk_p = rtk_p.where(rtk_p != 0.)
    rtk_p.dropna(inplace=True)    
    return(rtk_p)


def interpolate_time(df):
    # get a clock tick near sec_elapsed = 0
    in_flight  = df[df["sec_elapsed"] > 0.]
    first_time = in_flight["datetimestamp"].values[0]
    tick_row = 0
    # find the first in-flight clock tick
    while in_flight["datetimestamp"].values[tick_row] == first_time:
        tick_row += 1

    tick_num = in_flight["tick"].values[tick_row]

    print(tick_row)
       
    # we are declaring this row to be t=0
    # shift sec_elapsed to fit this
    sec_offset = -1.*in_flight['sec_elapsed'].values[tick_row]
    print(sec_offset)

    tick_row = tick_row + (len(df["tick"].values) - len(in_flight["tick"].values))
    init_time = np.datetime64(df["datetimestamp"].values[tick_row])
    print(init_time)
    print("INIT TIME SET AT "+str(df['tick'].values[tick_row]))
    if (df["tick"].values[tick_row] != tick_num):
        print("alignment check failed")
    
    secs = np.add(df["sec_elapsed"].values, sec_offset)
    deltas = []
    i=0
    for row in secs:
        sec = floor(row)
        milli = int((row - sec)*1000)
        if(milli > 1000): print(milli)
        sec_delta = np.timedelta64(sec, 's')
        mil_delta = np.timedelta64(milli, 'ms')
        delta = sec_delta + mil_delta
        deltas.append(delta)
        i+=1
    
    moved_times = []
    for i in range(len(deltas)):
        moved_times.append(init_time + deltas[i])
    
    df = df.assign(timestamp = moved_times)
    return(df)

# reads the given columns from a file and returns a pandas dataframe
def process_files(filenames, columns=COL_LIST):
    """
    Processes raw CSVs into much smaller files.

    Accepts multiple files in chronological order, 
    and produces a single file containing all of the data. 

    This should be run once on each new input file.

    Parameters
    ----------
    filenames : list of str
        |  Filenames to load
        |  If you are using one file, it must be passed as ['file']
        |  Also supports multiple files: ['file1', 'file2', etc]
        |  Files should be listed in chronological order

    columns : list of str, optional
        |  Columns to read
        |  load_csv.py has a sane default list

    Returns
    -------
    pd.DataFrame
        A new DataFrame with your data


    """
    frames = []
    for name in filenames:
        df = load_columns(name)
        df = interpolate_time(df)
        frames.append(df)
    print("STAPLING...")
    df = pd.DataFrame()
    for frame in frames:
        df = pd.concat([df, frame], ignore_index=True)

    if(len(frames) > 1):
        if((len(frames[0]['tick'].values)+len(frames[1]['tick'].values))==len(df['tick'].values)):
            print("STAPLING SUCCEEDED")
        else: print("ERROR: PLEASE CLEAR STAPLER JAM")

    # build a file name:
    name = ""
    for word in filenames:
        name = name + word
        name = name + "_"
    print(name)
    output_df(df, name)
    return df
# outputs the given dataframe as CSV and NPZ
def output_df(df, name):
    """
    |  Outputs your DataFrame to CSV and NPZ
    |  The CSV file will contain all columns
    |  The NPZ file will contain only timestamp, Lat, Lon, and hmsl

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to convert
    name : String
        The name of the data file, for naming output files

    Notes
    -----
    |  Saves NPZ to ./npz/nameDrone_Coordinates.npz
    |  Saves CSV to ./csv_out/nameprocessed.csv

    """
    # output a few columns of the stapled frame as a NPZ file
    np.savez("./npz/"+name+"Drone_Coordinates.npz",timestamp=df['timestamp'].values,Lat=df['Lat'].values,Lon=df['Lon'].values,hmsl=df['hmsl'].values)
    print("Saved NPZ")

    # output the whole stapled frame as a CSV file
    df.to_csv("./csv_out/"+name+"processed.csv")
    print("Saved CSV")















