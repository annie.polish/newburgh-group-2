############################
#       Annie Polish       #
#       Summer 2019        #
# Functions to add columns #
############################

# Each function accepts a dataframe, adds some columns, and returns the dataframe

import numpy as np
import pandas as pd
import pymap3d as pm
from math import *
from datetime import datetime
from columns import *

# isMoving is a guess at whether the drone is currently moving
def fill_moving(df: pd.DataFrame, xy_thresh= 0.00000003, h_thresh=0.005, win = 50):
    """
    Fills a column with a bool indicating if the drone is moving

    This adds the following columns:
        * std_lat : float, latitude standard deviation
        * std_lon : float, longitude standard deviation
        * std_hmsl : float, height standard deviation
        * isMoving : bool


    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to examine and add a column
    xy_thresh : float, optional
        In units of degrees of lat/lon.
        If coordinates exit this radius, they are moving
    h_thresh : float, optional
        In units of meters. 
        If height differs from the average by this much, the point is moving
    win : int, optional
        Number of points for the rolling average window


    Returns
    -------
    pd.DataFrame
        The DataFrame you passed in, with one extra column

    """

    latSer = pd.Series(df["Lat"].values, index=df.index)
    lonSer = pd.Series(df["Lon"].values, index=df.index)
    hmslSer= pd.Series(df["hmsl"].values,index=df.index)
    df = df.assign(std_lat = latSer.rolling(50, center=True).std())
    df = df.assign(std_lon = lonSer.rolling(50, center=True).std())
    df = df.assign(std_hmsl = hmslSer.rolling(50, center=True).std())
    df = df.assign(isMoving = np.logical_or(np.greater(df["std_lat"].values,xy_thresh), 
        np.greater(df["std_lon"].values, xy_thresh),
        np.greater(df["std_hmsl"].values, h_thresh)))
    return df

# fill in moving averages
def fill_avMov(df: pd.DataFrame, win = 20):
    """
    Fills a column with a moving average in each coordinate

    This adds the following columns:
        * avMovLat : float, latitude moving average
        * avMovLon : float, longitude moving average
        * avMovHmsl : float, height moving average

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to analyze
    win : int, optional
        The window for the rolling average, in number of points

    Returns
    -------
    pd.DataFrame
        The DataFrame you passed in, with some extra columns

    """


    latSer = pd.Series(df["Lat"].values, index=df.index)
    lonSer = pd.Series(df["Lon"].values, index=df.index)
    hmslSer= pd.Series(df["hmsl"].values,index=df.index)
    df = df.assign(avMovLat = latSer.rolling(win, center=True).mean())
    df = df.assign(avMovLon = lonSer.rolling(win, center=True).mean())
    df = df.assign(avMovHmsl = hmslSer.rolling(win, center=True).mean())
    return df

# slice up the dataframe
def chop_time(df, startTime, endTime):
    """
    Function to take a time slice of a DataFrame

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to slice
    startTime : np.datetime64
        Time where your new DataFrame begins
    endTime : np.datetime64
        Time where your new DataFrame ends

    Returns
    -------
    pd.DataFrame
        The slice of DataFrame in the given time interval

    """

    df_slice = df[df["timestamp"] > startTime]
    df_slice = df_slice[df_slice["timestamp"] < endTime]
    return(df_slice)

# uses pymap3d for coordinate transformation
def make_ENU(df, startTime=0, endTime=0, lat_origin=0, lon_origin=0, height_origin=0):
    """
    Moves the DataFrame into a local coordinate system

    Requires an origin point of the coordinate system.
    This function supports two ways of specifying the origin:
    1. Using startTime and endTime to give a time interval over which you average
    2. Directly providing a set of coordinates in lat, lon, hmsl

    This adds the following columns:
     * east : float, UNIT TBD
     * north : float, UNIT TBD
     * up : float, UNIT TBD

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to analyze
    startTime : np.datetime64, optional
        Beginning of averaging interval for determining the origin
    endTime : np.datetime64, optional
        End of averaging interval for determining the origin
    lat_origin : float, optional
        Latitude of the origin point
    lon_origin : float, optional
        Longitude of the origin point
    height_origin : float, optional
        Height above MSL of the origin point

    Returns
    -------
    pd.DataFrame
        The DataFrame you passed in, with some extra columns 


    """
    if(startTime == 0):
        startTime = df['timestamp'].values[0]
    if(endTime == 0):
        endTime = df['timestamp'].values[-1]
    # find the origin of our transformed coordinate system
    # z first:
    if (height_origin == 0):
        height_origin = np.mean(df[df["sec_elapsed"] < 0]["hmsl"].values)
        if (height_origin == 0 or np.isnan(height_origin)):
            height_origin = np.min(df['hmsl'].values)
        print(height_origin)
    
    # lat+long:
    df_slice = chop_time(df, startTime, endTime)
    if (lat_origin == 0): lat_origin = np.mean(df_slice["Lat"].values)
    if (lon_origin == 0): lon_origin = np.mean(df_slice["Lon"].values)
    print(lat_origin)
    print(lon_origin)
    
    east, north, up = pm.geodetic2enu(df["Lat"].values, df["Lon"].values, df["hmsl"].values,
                                      lat_origin, lon_origin, height_origin)
    
    df = df.assign(east = east)
    df = df.assign(north = north)
    df = df.assign(up = up)
    return(df)


def make_az_el(df, startTime=0, endTime=0, lat_origin=0, lon_origin=0, height_origin=0):
    """
    Moves the DataFrame into a local coordinate system

    Requires an origin point of the coordinate system.
    This function supports two ways of specifying the origin:
    1. Using startTime and endTime to give a time interval over which you average
    2. Directly providing a set of coordinates in lat, lon, hmsl

    This adds the following columns:
     * az : float, degrees
     * el : float, degrees
     * ran : float, UNIT TBD

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to analyze
    startTime : np.datetime64, optional
        Beginning of averaging interval for determining the origin
    endTime : np.datetime64, optional
        End of averaging interval for determining the origin
    lat_origin : float, optional
        Latitude of the origin point
    lon_origin : float, optional
        Longitude of the origin point
    height_origin : float, optional
        Height above MSL of the origin point

    Returns
    -------
    pd.DataFrame
        The DataFrame you passed in, with some extra columns 


    """
    if(startTime == 0):
        startTime = df['timestamp'].values[0]
    if(endTime == 0):
        endTime = df['timestamp'].values[-1]
    # find the origin of our transformed coordinate system
    # z first:
    if (height_origin == 0):
        height_origin = np.mean(df[df["sec_elapsed"] < 0]["hmsl"].values)
        if (height_origin == 0 or np.isnan(height_origin)):
            height_origin = np.min(df['hmsl'].values)
        print(height_origin)
    
    # lat+long:
    df_slice = chop_time(df, startTime, endTime)
    if (lat_origin == 0): lat_origin = np.mean(df_slice["Lat"].values)
    if (lon_origin == 0): lon_origin = np.mean(df_slice["Lon"].values)
    print(lat_origin)
    print(lon_origin)
    
    az, el, ran = pm.geodetic2aer(df["Lat"].values, df["Lon"].values, df["hmsl"].values,
                       lat_origin, lon_origin, height_origin)
    
    df = df.assign(az = az)
    df = df.assign(el = el)
    df = df.assign(ran = ran)
    return(df)

