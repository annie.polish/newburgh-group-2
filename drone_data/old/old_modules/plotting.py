######################################
#           Annie Polish             #
#           Summer 2019              #
# MPL and Cartopy plotting functions #
######################################


import matplotlib as mpl
import matplotlib.cm
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from mpl_toolkits.mplot3d import Axes3D 
import numpy as np
import pandas as pd
import pymap3d as pm
from math import *
from datetime import datetime
from matplotlib.transforms import offset_copy
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import geopy.distance
from scalebar import scale_bar #requires the existance of scalebar.py


# defines some global properties of plots
BORDER = 0.00003 # sets the border around the dataset, in degrees of lat/long
# your mapbox.com API token
MAPBOX_KEY = "pk.eyJ1IjoiYW5uaWVwb2xpc2giLCJhIjoiY2p5b3BwdXl3MTdhdzNjdDRjbGw5MWJ6ciJ9.01NjskBuc2SQcm5QjbyLwA"
MAPBOX_STYLE = "cjypy1k7x0ru71cjva7cs5iwz"
MAPBOX_USERNAME = "anniepolish"
PLOT_BG = cimgt.MapboxStyleTiles(MAPBOX_KEY, MAPBOX_USERNAME, MAPBOX_STYLE)
date_format = dates.DateFormatter("%H:%M:%S")

# interface for changing the plot border
def set_border(border):
    """
    Changes the border of plots
    
    Parameters
    ----------
    border : float
        Size of the border in degrees of lat/lon.
        Reasonable sizes are 0.00001 to 0.00009

    """

    if (border < 0.1 and border >= 0):
        BORDER = border
    else:
        print("Error: bad border value")
        return -1
    return 0


# plot the flight path over satellite imagery
def plot_satellite(df, name, figsize=(10, 10), color='c', avMov=True):
    """
    Plots the drone's flight path over a satellite map

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for labeling and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    color : String, optional
        Passed to matplotlib. Defaults to 'c'
    avMov : bool, optional
        Moving average is used if True. 
        Raw coordinates are used if False. 
        Defaults to True

    Notes
    -----
    Saves plot to ./plots/name.png

    """
    # make sure the expected columns exist
    if(not "Lat" in df.columns or not "Lon" in df.columns):
        print("Error: bad dataframe - Lat/Lon not found")
        return -1
    if(not "timestamp" in df.columns):
        print("Error: bad dataframe - timestamp not found")
        return -1
    if(avMov and (not "avMovLat" in df.columns or not "avMovLon" in df.columns)):
        print("Error: bad dataframe - avMov not found")
        print("Specify avMov=False if you do not have a moving average")
        return -1
   
    # find the date for the plot title
    date = str(df["timestamp"].values[0])[:10]

    # begin the plot
    plt.rcParams.update({'font.size': 16})
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(1, 1, 1, projection=PLOT_BG.crs) # add a subplot in the coordinate system of the tiles
    ax.set_extent([min(df["Lon"].values)-BORDER, max(df["Lon"].values)+BORDER, 
        min(df["Lat"].values)-BORDER, max(df["Lat"].values)+BORDER], crs=ccrs.Geodetic()) # set plot size
    ax.add_image(PLOT_BG, 20, interpolation='spline36') # go get some map tiles
    # plot the RTK gps data
    if(avMov):
        plt.plot(df["avMovLon"].values, df["avMovLat"].values, color, linewidth=2, transform=ccrs.Geodetic(), label="RTK")
    else:
        plt.plot(df["Lon"].values, df["Lat"].values, color, linewidth=2, transform=ccrs.Geodetic, label="RTK")
    # label stuff and save
    plt.xlabel("Longitude")
    plt.ylabel("Latitude")
    plt.title("RTK data - "+name+" - "+str(date))
    fig.canvas.draw()
    plt.tight_layout()
    plt.savefig("./plots/"+name+".png")
    plt.show()


# plot points over satellite imagery
def plot_points(df, name, figsize=(10, 10), legend=True, errorbars=False):
    """
    Plots a list of points over a satellite map

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for labeling and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    legend : bool, optional
        Turns the plot legend on or off. Defaults to True
    errorbars : bool, optional
        Turns error bars on or off. Defaults to False

    Notes
    -----
    Saves plot to ./plots/points_name.png


    """
    if(not 'point' in df.columns):
        print("Error: no points")
        return -1
    
    # find the date for the plot title
    date = str(df["timestamp"].values[0])[:10]

    #plot!
    fig = plt.figure(figsize=figsize)
    cm = plt.get_cmap('tab20')
    ax = fig.add_subplot(1,1,1, projection=PLOT_BG.crs)
    ax.set_prop_cycle(color=[cm(1.*i/20) for i in range(20)])
    ax.set_extent([min(df["Lon"].values)-BORDER, max(df["Lon"].values)+BORDER, 
        min(df["Lat"].values)-BORDER, max(df["Lat"].values)+BORDER], crs=ccrs.Geodetic())
    ax.add_image(PLOT_BG, 20, interpolation='spline36') # go get some map tiles
    # plot each point
    for i in range(max(df['point'].values)):
        lat = df[df['point'] == i]['Lat'].values
        lon = df[df['point'] == i]['Lon'].values
        plt.plot(lon, lat, label=str(i)+"-"+str(df[df['point']==i]['timestamp'].values[0])[11:19], transform=ccrs.Geodetic(), linewidth=3.0)
        if(errorbars): plt.errorbar(np.mean(lat), np.mean(lon), xerr=np.std(lat), yerr=np.std(lon), color = 'm', zorder=10)
    # label stuff and save
    plt.xlabel("Longitude")
    plt.ylabel("Latitude")
    if(legend): plt.legend()
    plt.title("Stationary points - "+name+" "+str(date))
    plt.grid()
    fig.canvas.draw()
    plt.tight_layout()
    plt.savefig("./plots/points_"+name+".png")
    print("saved plot")
    plt.show()


# plot latitude vs time
def plot_latitude(df, name, figsize=(10,10), color='C0'):
    """
    Plots latitude vs time

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for naming and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    color : String, optional
        Passed to matplotlib. Defaults to 'C0'

    Notes
    -----
    Saves plot to ./plots/name_lat.png
    """
    plt.rcParams.update({'font.size': 16})
    fig = plt.figure(figsize=figsize)
    ax = plt.gca()
    ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)
    ax.yaxis.set_minor_formatter(LATITUDE_FORMATTER)
    ax.xaxis.set_major_formatter(date_format)
    ax.xaxis_date()
    plt.title("Latitude vs UTC Time")
    plt.ylim(min(df['Lat'].values), max(df['Lat'].values))
    plt.plot(df['timestamp'].values, df['Lat'].values, color)
    plt.grid()
    plt.xlim(min(df['timestamp'].values),max(df["timestamp"].values))
    plt.xticks(rotation=50)
    #plt.tight_layout()
    plt.savefig("./plots/"+name+"_lat.png")
    plt.show()


# plot longitude vs time
def plot_longitude(df, name, figsize=(10,10), color='C0'):
    """
    Plots longitude vs time

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for naming and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    color : String, optional
        Passed to matplotlib. Defaults to 'C0'

    Notes
    -----
    Saves plot to ./plots/name_lon.png
    """

    plt.rcParams.update({'font.size': 16})
    fig = plt.figure(figsize=figsize)
    plt.title("Longitude vs UTC Time")
    plt.ylim(min(df['Lon'].values), max(df['Lon'].values))
    plt.plot(df['timestamp'].values, df['Lon'].values, color)
    plt.grid()
    ax = plt.gca()
    ax.yaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_minor_formatter(LONGITUDE_FORMATTER)
    ax.xaxis.set_major_formatter(date_format)
    plt.xlim(min(df['timestamp'].values),max(df["timestamp"].values))
    plt.xticks(rotation=50)
    #plt.tight_layout()
    plt.savefig("./plots/"+name+"_lon.png")
    plt.show()


# plot height vs time
def plot_height(df, name, figsize=(10,10), color='C0'):
    """
    Plots height vs time

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for naming and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    color : String, optional
        Passed to matplotlib. Defaults to 'C0'

    Notes
    -----
    Saves plot to ./plots/name_hmsl.png
    """

    fig = plt.figure(figsize=figsize)
    plt.title("Height vs Elapsed Time")
    plt.ylim(min(df["hmsl"].values), max(df["hmsl"].values))
    plt.plot(df["timestamp"].values, df["hmsl"].values, color)
    plt.grid()
    plt.gca().xaxis.set_major_formatter(date_format)
    plt.xticks(rotation=50)
    plt.savefig("./plots/"+name+"_hmsl.png")
    plt.show()


# plot moving and not moving for point tagging
def plot_moving(df, name, figsize=(10,10), view="Lon"):
    """
    Plots a coordinate vs time, with different colors when moving or not moving

    This is used to manually tag points.
    It can only be run after filling the isMoving column

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for naming and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    view : String, optional
        Column of the dataframe to plot. 
        Defaults to 'Lon'. 
        Other useful options are 'Lat' and 'hmsl'

    Notes
    -----
    This one does not save plots


    """
    if(not "isMoving" in df.columns):
        print("Error: no movement column")
        return -1
    if(not view in df.columns):
        print("Error: invalid view")
        return -1
    rtk_mov = df[df["isMoving"] == True]
    rtk_stop = df[df["isMoving"] == False]

    fig = plt.figure(figsize=figsize)
    plt.title(view+" vs Seconds Elapsed")
    plt.ylim(min(df[view].values), max(df[view].values))
    plt.scatter(rtk_mov['timestamp'].values, rtk_mov[view].values, s=2, color='c')
    plt.scatter(rtk_stop['timestamp'].values, rtk_stop[view].values, s=2, color='m')
    plt.grid()
    ax = plt.gca()
    ax.yaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_minor_formatter(LONGITUDE_FORMATTER)
    ax.xaxis.set_major_formatter(date_format)
    plt.xlim(min(df['timestamp'].values),max(df["timestamp"].values))
    plt.tight_layout()
    plt.show()





