######################################
#           Annie Polish             #
#           Summer 2019              #
#  Drone data processing functions   #
######################################

# REFACTORED!!!
# New and ~~classy~~

import matplotlib as mpl
import matplotlib.cm
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from mpl_toolkits.mplot3d import Axes3D 
import numpy as np
import pandas as pd
import pymap3d as pm
from scipy.stats import norm
from math import *
from datetime import datetime
from matplotlib.transforms import offset_copy
from matplotlib.ticker import FormatStrFormatter
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import geopy.distance
from scalebar import scale_bar #requires the existance of scalebar.py


class DroneData:
    # class variables defining some constants used for satellite plots
    MAPBOX_KEY = "pk.eyJ1IjoiYW5uaWVwb2xpc2giLCJhIjoiY2p5b3BwdXl3MTdhdzNjdDRjbGw5MWJ6ciJ9.01NjskBuc2SQcm5QjbyLwA"
    MAPBOX_STYLE = "cjypy1k7x0ru71cjva7cs5iwz"
    MAPBOX_USERNAME = "anniepolish"
    PLOT_BG = cimgt.MapboxStyleTiles(MAPBOX_KEY, MAPBOX_USERNAME, MAPBOX_STYLE)
    BORDER = 0.00003

    def __init__(self, df:pd.DataFrame, name, mode='auto'): # mode may be auto or manual
        self.df = df
        self.name = name
        self.maptype= "mapbox" # map tile source
 
    # slice up the dataframe and return a new DroneData
    def chop_time(self, startTime, endTime):
        """
        Function to take a time slice of a DataFrame

        Parameters
        ----------
        startTime : np.datetime64
            Time where your new DataFrame begins
        endTime : np.datetime64
            Time where your new DataFrame ends

        Returns
        -------
        pd.DataFrame
            The slice of DataFrame in the given time interval

        """
        
        df_slice = self.df[self.df["timestamp"] > startTime]
        df_slice = df_slice[df_slice["timestamp"] < endTime]
        return(DroneData(df_slice, self.name)) # return new DroneData instance

   
    # METHODS WHICH MODIFY COLUMNS
    # isMoving is a guess at whether the drone is currently moving
    def fill_moving(self, method='av', vel_thresh=1.0, xy_thresh= 0.00000003, h_thresh=0.005, win = 50):
        """
        Fills a column with a bool indicating if the drone is moving

        This adds the following columns:
            * std_lat : float, latitude standard deviation
            * std_lon : float, longitude standard deviation
            * std_hmsl : float, height standard deviation
            * isMoving : bool


        Parameters
        ----------
        xy_thresh : float, optional
            In units of degrees of lat/lon.
            If coordinates exit this radius, they are moving
        h_thresh : float, optional
            In units of meters. 
            If height differs from the average by this much, the point is moving
        win : int, optional
            Number of points for the rolling average window

        """
        df = self.df

        latSer = pd.Series(df["Lat"].values, index=df.index)
        lonSer = pd.Series(df["Lon"].values, index=df.index)
        hmslSer= pd.Series(df["hmsl"].values,index=df.index)
        df = df.assign(std_lat = latSer.rolling(50, center=True).std())
        df = df.assign(std_lon = lonSer.rolling(50, center=True).std())
        df = df.assign(std_hmsl = hmslSer.rolling(50, center=True).std())
    
        if method == 'av':
            df = df.assign(isMoving = np.logical_or(np.greater(df["std_lat"].values,xy_thresh), 
                np.greater(df["std_lon"].values, xy_thresh),
                np.greater(df["std_hmsl"].values, h_thresh)))
        elif method == 'vel': 
            df = df.assign(isMoving = np.greater(df['vel'].values, vel_thresh))
        else: print('INVALID MOVING METHOD')
        self.df = df

    # fill in moving averages
    def fill_avMov(self, win = 20):
        """
        Fills a column with a moving average in each coordinate

        This adds the following columns:
            * avMovLat : float, latitude moving average
            * avMovLon : float, longitude moving average
            * avMovHmsl : float, height moving average

        Parameters
        ----------
        win : int, optional
            The window for the rolling average, in number of points

        Returns
        -------
        pd.DataFrame
            The DataFrame you passed in, with some extra columns

        """
        df = self.df

        latSer = pd.Series(df["Lat"].values, index=df.index)
        lonSer = pd.Series(df["Lon"].values, index=df.index)
        hmslSer= pd.Series(df["hmsl"].values,index=df.index)
        df = df.assign(avMovLat = latSer.rolling(win, center=True).mean())
        df = df.assign(avMovLon = lonSer.rolling(win, center=True).mean())
        df = df.assign(avMovHmsl = hmslSer.rolling(win, center=True).mean())
        self.df = df

    # uses pymap3d for coordinate transformation
    def make_ENU(self, lat_origin=0, lon_origin=0, height_origin=0):
        """ 
        Moves the DataFrame into a local coordinate system

        Requires an origin point of the coordinate system.
        This function supports two ways of specifying the origin:
        1. Using startTime and endTime to give a time interval over which you average
        2. Directly providing a set of coordinates in lat, lon, hmsl

        This adds the following columns:
        * east : float, UNIT TBD
        * north : float, UNIT TBD
        * up : float, UNIT TBD

        Parameters
        ----------
        startTime : np.datetime64, optional
            Beginning of averaging interval for determining the origin
        endTime : np.datetime64, optional
            End of averaging interval for determining the origin
        lat_origin : float, optional
            Latitude of the origin point
        lon_origin : float, optional
            Longitude of the origin point
        height_origin : float, optional
            Height above MSL of the origin point

        Returns
        -------
        pd.DataFrame
            The DataFrame you passed in, with some extra columns 


        """
    
        df = self.df
        # find the origin of our transformed coordinate system
        # z first:
        if (height_origin == 0):
            height_origin = np.mean(df[df["sec_elapsed"] < 0]["hmsl"].values)
        if (height_origin == 0 or np.isnan(height_origin)):
            height_origin = np.min(df['hmsl'].values)
        print(height_origin)
    
        # lat+long:
        if (lat_origin == 0): lat_origin = np.mean(df["Lat"].values)
        if (lon_origin == 0): lon_origin = np.mean(df["Lon"].values)
        print(lat_origin)
        print(lon_origin)
    
        east, north, up = pm.geodetic2enu(df["Lat"].values, df["Lon"].values, df["hmsl"].values,
                                      lat_origin, lon_origin, height_origin)
    
        df = df.assign(east = east)
        df = df.assign(north = north)
        df = df.assign(up = up)
        self.df = df


    def make_az_el(self, startTime=0, endTime=0, lat_origin=0, lon_origin=0, height_origin=0):
        """
        Moves the DataFrame into a local coordinate system

        Requires an origin point of the coordinate system.
        This function supports two ways of specifying the origin:
        1. Using startTime and endTime to give a time interval over which you average
        2. Directly providing a set of coordinates in lat, lon, hmsl

        This adds the following columns:
         * az : float, degrees
         * el : float, degrees
         * ran : float, UNIT TBD

        Parameters
        ----------
        startTime : np.datetime64, optional
            Beginning of averaging interval for determining the origin
        endTime : np.datetime64, optional
            End of averaging interval for determining the origin
        lat_origin : float, optional
            Latitude of the origin point
        lon_origin : float, optional
            Longitude of the origin point
        height_origin : float, optional
            Height above MSL of the origin point

        Returns
        -------
        pd.DataFrame
            The DataFrame you passed in, with some extra columns 


        """
        df = self.df
        if(startTime == 0):
            startTime = df['timestamp'].values[0]
        if(endTime == 0):
            endTime = df['timestamp'].values[-1]
        # find the origin of our transformed coordinate system
        # z first:
        if (height_origin == 0):
            height_origin = np.mean(df[df["sec_elapsed"] < 0]["hmsl"].values)
            if (height_origin == 0 or np.isnan(height_origin)):
                height_origin = np.min(df['hmsl'].values)
            print(height_origin)
        
        # lat+long:
        df_slice = chop_time(df, startTime, endTime)
        if (lat_origin == 0): lat_origin = np.mean(df_slice["Lat"].values)
        if (lon_origin == 0): lon_origin = np.mean(df_slice["Lon"].values)
        print(lat_origin)
        print(lon_origin)
        
        az, el, ran = pm.geodetic2aer(df["Lat"].values, df["Lon"].values, df["hmsl"].values,
                           lat_origin, lon_origin, height_origin)
        
        df = df.assign(az = az)
        df = df.assign(el = el)
        df = df.assign(ran = ran)
        self.df = df


    # METHODS WHICH PLOT THE DATAFRAME
    # general plotter function
    def plot(self, view, figsize=(10,10)):
        if not view in self.df.columns:
            print("Error: Invalid View")
            return -1
        plt.rcParams.update({'font.size': 16})
        fig = plt.figure(figsize=figsize)
        ax = plt.gca()
        
        #TODO conditional axis formatting
        if view=="Lat":
            ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)
            ax.yaxis.set_minor_formatter(LATITUDE_FORMATTER)
        if view=="Lon":
            ax.yaxis.set_major_formatter(LONGITUDE_FORMATTER)
            ax.yaxis.set_minor_formatter(LONGITUDE_FORMATTER)

        date_format = dates.DateFormatter("%H:%M:%S")
        ax.xaxis.set_major_formatter(date_format)
        ax.xaxis_date()
        plt.title(view+" vs UTC Time")
        plt.plot(self.df['timestamp'].values, self.df[view].values)
        plt.grid()
        plt.xlim(min(self.df['timestamp'].values),max(self.df["timestamp"].values))
        plt.xticks(rotation=50)
        plt.tight_layout()
        plt.savefig("./plots/"+self.name+"_"+view+".png")
        plt.show()

    # plot the flight path over satellite imagery
    def plot_satellite(self, figsize=(10, 10), color='c', avMov=False):
       
        # find the date for the plot title
        date = str(self.df["timestamp"].values[0])[:10]

        # begin the plot
        plt.rcParams.update({'font.size': 16})
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(1, 1, 1, projection=self.PLOT_BG.crs) # add a subplot in the coordinate system of the tiles
        ax.set_extent([min(self.df["Lon"].values)-self.BORDER, max(self.df["Lon"].values)+self.BORDER, 
            min(self.df["Lat"].values)-self.BORDER, max(self.df["Lat"].values)+self.BORDER], crs=ccrs.Geodetic()) # set plot size
        ax.add_image(self.PLOT_BG, 20, interpolation='spline36') # go get some map tiles
        # plot the RTK gps data
        if(avMov):
            plt.plot(self.df["avMovLon"].values, self.df["avMovLat"].values, color, linewidth=2, transform=ccrs.Geodetic(), label="RTK")
        else:
            plt.plot(self.df["Lon"].values, self.df["Lat"].values, color, linewidth=2, transform=ccrs.Geodetic(), label="RTK")
        # label stuff and save
        plt.xlabel("Longitude")
        plt.ylabel("Latitude")
        plt.title("RTK data - "+self.name+" - "+str(date))
        fig.canvas.draw()
        plt.tight_layout()
        plt.savefig("./plots/"+self.name+".png")
        plt.show()


    # plot points over satellite imagery
    def plot_points(self, figsize=(10, 10), legend=True, errorbars=False, tiles=True):
        if(not 'point' in self.df.columns):
            print("Error: no points")
            return -1
        
        # find the date for the plot title
        date = str(self.df["timestamp"].values[0])[:10]

        #plot!
        fig = plt.figure(figsize=figsize)
        cm = plt.get_cmap('tab20')
        ax = fig.add_subplot(1,1,1, projection=self.PLOT_BG.crs)
        #ax.set_prop_cycle(color=[cm(1.*i/20) for i in range(20)])
        ax.set_extent([self.point_coords(0)[1]-self.BORDER/4., self.point_coords(0)[1]+self.BORDER/4., 
            self.point_coords(0)[0]-self.BORDER/4., self.point_coords(0)[0]+self.BORDER/4.], crs=ccrs.Geodetic())
        if tiles:
            ax.add_image(self.PLOT_BG, 20, interpolation='spline36') # go get some map tiles
        # plot each point
        for i in range(0,max(self.df['point'].values)+1):
            print("plotting point "+str(i))
            lat = self.df[self.df['point'] == i]['Lat'].values
            lon = self.df[self.df['point'] == i]['Lon'].values
            plt.plot(lon, lat, label=str(i)+"-"+str(self.df[self.df['point']==i]['timestamp'].values[0])[11:19], transform=ccrs.Geodetic(), linewidth=3.0)
            if(errorbars): plt.errorbar(np.mean(lat), np.mean(lon), xerr=np.std(lat), yerr=np.std(lon), color = 'm', zorder=10)
        # label stuff and save
        plt.xlabel("Longitude")
        plt.ylabel("Latitude")
        if(legend): plt.legend()
        plt.title("Stationary points - "+self.name+" "+str(date))
        plt.grid()
        fig.canvas.draw()
        plt.tight_layout()
        plt.savefig("./plots/points_"+self.name+".png")
        print("saved plot")
        plt.show()

    # make a 2d scatter with 1d histograms and fits for a point
    # initially for nov 2019 proposal
    def point_hist(self, point):
        plt.rcParams.update({'font.size': 19})

        df = self.df
        df_slice = df[df["point"] == point]
        lat_arr = df_slice["Lat"].values
        lon_arr = df_slice["Lon"].values
        
        # convert each of the latitude and longitude arrays into a displacement from mean, in meters
        center_lat = np.mean(lat_arr)
        center_lon = np.mean(lon_arr)
        lat_disp, lon_disp = [], []
        for lat in lat_arr:
            disp = geopy.distance.distance((center_lat, center_lon), (lat, center_lon)).m*100.
            if (lat < center_lat): disp = -1.*disp
            lat_disp.append(disp)
        for lon in lon_arr:
            disp = geopy.distance.distance((center_lat, center_lon), (center_lat, lon)).m*100.
            if (lon < center_lon): disp = -1.*disp
            lon_disp.append(disp)    

        x = lat_disp
        y = lon_disp

        # definitions for the axes
        left, width = 0.1, 0.65
        bottom, height = 0.1, 0.65
        spacing = 0.005
        label_gap = 0.02


        rect_scatter = [label_gap+left, bottom, width, height]
        rect_histx = [label_gap+left, bottom + height + spacing, width, 0.2]
        rect_histy = [label_gap+left + width + spacing, bottom, 0.2, height]

        # start with a rectangular Figure
        plt.figure(figsize=(8, 8))

        ax_scatter = plt.axes(rect_scatter)
        ax_scatter.tick_params(direction='in', top=True, right=True)
        ax_scatter.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
        ax_scatter.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
        ax_histx = plt.axes(rect_histx)
        ax_histx.tick_params(direction='in', labelbottom=False, labelleft=False)
        ax_histy = plt.axes(rect_histy)
        ax_histy.tick_params(direction='in', labelleft=False, labelbottom=False)

        # the scatter plot:
        ax_scatter.scatter(x, y)

        # now determine nice limits by hand:
        binwidth = 0.04
        lim = np.ceil(np.abs([x, y]).max() / binwidth) * binwidth
        ax_scatter.set_xlim((-lim, lim))
        ax_scatter.set_ylim((-lim, lim))
        ax_scatter.yaxis.set_ticks([-0.5,0,0.5])
        #ax_scatter.grid()


        bins = np.arange(-lim, lim + binwidth, binwidth)
        ax_histx.hist(x, bins=bins, density=True)
        ax_histy.hist(y, bins=bins, orientation='horizontal', density=True)

        ax_histx.set_xlim(ax_scatter.get_xlim())
        ax_histy.set_ylim(ax_scatter.get_ylim())
        
        # make and add gaussian fits
        lat_mu, lat_sigma = norm.fit(lat_disp)
        lon_mu, lon_sigma = norm.fit(lon_disp)
        ax_histx.plot(bins, norm.pdf(bins, lat_mu, lat_sigma), '--', color='orange', linewidth=4)
        ax_histy.plot(norm.pdf(bins, lon_mu, lon_sigma), bins, '--', color='orange', linewidth=4)
        ax_histx.text(0.35, 1.4, "$\sigma$: %.3fcm"%lat_sigma)
        ax_histy.text(0.12, 0.77, "$\sigma$: %.3fcm"%lon_sigma)
        
        # LABELS LABELS LABELS
        ax_scatter.set_xlabel("Latitude Displacement [cm]")
        ax_scatter.set_ylabel("Longitude Displacement [cm]")
        
        #plt.tight_layout()
        plt.title(self.name+" point "+str(point))

        plt.savefig("./plots/hist_"+self.name+"_%d_3up.png"%point)

        plt.show()

    # returns the statistics of the coordinates in a given time interval
    def interval_stats(self, startTime, endTime, avg=False):
        """
        |  Performs various statistics on the given time interval
        |  Converts these statistics from lat/lon to meters

        Parameters
        ----------
        df : pd.DataFrame
            The DataFrame to examine
        startTime : np.datetime64
            Start of measurement interval
        endTime : np.datetime64
            End of measurement interval
        avg : bool, optional
            Use moving average? Defaults to false

        Returns
        -------
        tuple
            Contains: (lat_distance, lon_distance, hmsl_dev, xy_distance)

        """


        print('hello stationary stats')
        # slice off part of the dataframe
        df_slice = self.df[self.df["timestamp"] > startTime]
        df_slice = df_slice[df_slice["timestamp"] < endTime]
        if (avg):
            lat_arr = df_slice["avMovLat"].values
            lon_arr = df_slice["avMovLon"].values
            hmsl_arr= df_slice["avMovHmsl"].values
        else:
            lat_arr  = df_slice["Lat"].values
            lon_arr  = df_slice["Lon"].values
            hmsl_arr = df_slice["hmsl"].values
        # find max and min of each coordinate, and the distance that represents
        max_lat  = np.max(lat_arr)
        min_lat  = np.min(lat_arr)
        max_lon  = np.max(lon_arr)
        min_lon  = np.min(lon_arr)
        lat_dist = geopy.distance.distance((min_lat, lon_arr[0]), (max_lat, lon_arr[0])).m
        lon_dist = geopy.distance.distance((lat_arr[0], min_lon), (lat_arr[0], max_lon)).m
        print("Max diagonal distance: "+str(sqrt(lat_dist**2 + lon_dist**2))+" meters")
        # find the standard deviation of each coordinate
        lat_dev  = np.std(lat_arr)
        lon_dev  = np.std(lon_arr)
        hmsl_dev = np.std(hmsl_arr)
        # convert stddev in degrees to stddev in meters
        startCoords = (lat_arr[0], lon_arr[0])
        latCoords = (lat_arr[0]+lat_dev, lon_arr[0])
        lonCoords = (lat_arr[0], lon_arr[0]+lon_dev)
        endCoords = (lat_arr[0]+lat_dev, lon_arr[0]+lon_dev)
        lat_dist = geopy.distance.distance(startCoords, latCoords).m
        lon_dist = geopy.distance.distance(startCoords, lonCoords).m
        xy_dist = geopy.distance.distance(startCoords, endCoords).m
        # print and return
        print("Latitude standard deviation: "+str(lat_dist)+" meters")
        print("Longitude standard deviation: "+str(lon_dist)+" meters")
        print("Height standard deviation: "+str(hmsl_dev)+" meters")
        print("Diagonal lat-lon stddev: "+str(xy_dist)+" meters")
        return (lat_dist, lon_dist, hmsl_dev, xy_dist)

    # returns the statistics of the coordinates in a given time interval
    def point_stats(self, point, avg=False):
        """
        |  Performs various statistics on the given point
        |  Converts these statistics from lat/lon to meters

        Parameters
        ----------
        df : pd.DataFrame
            The DataFrame to examine
        point : int
            Which point is being examined
        avg : bool, optional
            Use moving average? Defaults to false

        Returns
        -------
        tuple
            Contains: (lat_distance, lon_distance, hmsl_dev, xy_distance)

        """


        print('hello stationary stats')
        # slice off part of the dataframe
        df_slice = self.df[self.df["point"] == point]
        if (avg):
            lat_arr = df_slice["avMovLat"].values
            lon_arr = df_slice["avMovLon"].values
            hmsl_arr= df_slice["avMovHmsl"].values
        else:
            lat_arr  = df_slice["Lat"].values
            lon_arr  = df_slice["Lon"].values
            hmsl_arr = df_slice["hmsl"].values
        # find max and min of each coordinate, and the distance that represents
        max_lat  = np.max(lat_arr)
        min_lat  = np.min(lat_arr)
        max_lon  = np.max(lon_arr)
        min_lon  = np.min(lon_arr)
        lat_dist = geopy.distance.distance((min_lat, lon_arr[0]), (max_lat, lon_arr[0])).m
        lon_dist = geopy.distance.distance((lat_arr[0], min_lon), (lat_arr[0], max_lon)).m
        print("Max diagonal distance: "+str(sqrt(lat_dist**2 + lon_dist**2))+" meters")
        # find the standard deviation of each coordinate
        lat_dev  = np.std(lat_arr)
        lon_dev  = np.std(lon_arr)
        hmsl_dev = np.std(hmsl_arr)
        # convert stddev in degrees to stddev in meters
        startCoords = (lat_arr[0], lon_arr[0])
        latCoords = (lat_arr[0]+lat_dev, lon_arr[0])
        lonCoords = (lat_arr[0], lon_arr[0]+lon_dev)
        endCoords = (lat_arr[0]+lat_dev, lon_arr[0]+lon_dev)
        lat_dist = geopy.distance.distance(startCoords, latCoords).m
        lon_dist = geopy.distance.distance(startCoords, lonCoords).m
        xy_dist = geopy.distance.distance(startCoords, endCoords).m
        # print and return
        print("Latitude standard deviation: "+str(lat_dist)+" meters")
        print("Longitude standard deviation: "+str(lon_dist)+" meters")
        print("Height standard deviation: "+str(hmsl_dev)+" meters")
        print("Diagonal lat-lon stddev: "+str(xy_dist)+" meters")
        return (lat_dist, lon_dist, hmsl_dev, xy_dist)

    # measure the distance between two second markers
    def dist_between_times(self, startTime, endTime):
        """
        Reports the distance between two timestamps

        Parameters
        ----------
        df : pd.DataFrame
            The DataFrame to examine
        startTime : np.datetime64
            The time at which you measure your first point
        endTime : np.datetime64
            The time at which you measure your second point

        Returns
        -------
        float
            The distance between the two points, in meters

        """
        df_slice = self.df[self.df["timestamp"] > startTime]
        df_slice = df_slice[df_slice["timestamp"] < endTime]
        start_lat = df_slice.iloc[0]["Lat"]
        start_lon = df_slice.iloc[0]["Lon"]
        end_lat = df_slice.iloc[-1]["Lat"]
        end_lon = df_slice.iloc[-1]["Lon"]
        dist = geopy.distance.distance((start_lat, start_lon), (end_lat, end_lon)).m
        print(start_lat)
        print(start_lon)
        print(end_lat)
        print(end_lon)
        print("distance: "+str(dist))
        return(dist)

    # mark a point by expanding a bubble around a given origin
    def auto_point(self, timestamp, thresh = 0.2):
        """
        Records a point into the DataFrame

        Starts from the given timestamp, and expands the range out. 
        Range is expanded until the coordinates differ by more than the thresh. 
        The whole range then has its 'point' column updated

        Parameters
        ----------
        df : pd.DataFrame
            The DataFrame to examine and modify
        timestamp : np.datetime64
            The timestamp to use as the origin of the point
        thresh : float, optional
            Threshold bounding a point. Defaults to 0.2. 
            Increase this value to make points more error-tolerant. 
            Decrease it to make points cleaner

        Returns
        -------
        pd.DataFrame
            The DataFrame you passed in, with one more point tagged

        """
        # if the point column does not exist, make it
        if(not "point" in self.df.columns):
            self.df = self.df.assign(point = np.full_like(self.df['tick'].values, -1))
            
        # select a 10s window around the input point and average it
        est_delta = np.timedelta64(5, 's')
        df_tmp = chop_time(self.df, timestamp-est_delta, timestamp+est_delta)
        start_lat = np.mean(df_tmp["Lat"].values)
        start_lon = np.mean(df_tmp["Lon"].values)
        start_hmsl= np.mean(df_tmp["hmsl"].values)
        
        # find the starting location
        init_index = 0
        for stamp in self.df['timestamp'].values:
            if stamp < timestamp:
                init_index += 1
            else: break
        
        # find the left edge of the point
        left_offset = 0
        while(init_index - left_offset > 0):
            ind = init_index - left_offset
            lat = self.df['Lat'].values[ind]
            lon = self.df['Lon'].values[ind]
            hmsl= self.df['hmsl'].values[ind]
            
            dist = geopy.distance.distance((start_lat, start_lon), (lat, lon)).m
            if (dist < thresh): left_offset += 1
            else: break
        left_index = init_index - left_offset
        print(self.df['timestamp'].values[left_index])
        
        # find the right edge of the point
        right_offset = 0
        while(init_index + right_offset < len(self.df['tick'].values)-1):
            ind = init_index + right_offset
            lat = self.df['Lat'].values[ind]
            lon = self.df['Lon'].values[ind]
            hmsl= self.df['hmsl'].values[ind]
            
            dist = geopy.distance.distance((start_lat, start_lon), (lat, lon)).m
            if (dist < thresh): right_offset += 1
            else: break
        right_index = init_index + right_offset
        print(self.df['timestamp'].values[right_index])
        
        points_in = self.df['point'].values
        this_point = np.max(points_in) + 1
        print(np.max(points_in))
        for i in range(left_index, right_index):
            points_in[i] = this_point
        self.df = self.df.assign(point = points_in)
        print("Marked point "+str(this_point))

    # mark a point using timestamps
    def mark_point(self, time_start, time_end):
        print('marking point')
        mask = np.logical_and(
            self.df['timestamp'].values > time_start,
            self.df['timestamp'].values < time_end)

        print(mask)

        if not 'point' in self.df.columns:
            self.df = self.df.assign(point = np.full_like(self.df['tick'].values, -1))
            this_point = 0
        else:
            this_point = np.max(self.df['point'].values) + 1

        point_arr = self.df['point'].values
        
        self.df = self.df.assign(point = point_arr + (mask * (this_point+1)))

    # report the coordinate tuple of a point
    def point_coords(self, point, local=False):
        point_frame = self.df[self.df['point'] == point]
        if local:
            return((np.mean(point_frame['east'].values), np.mean(point_frame['north'].values)))
        else:
            return((np.mean(point_frame['Lat'].values), np.mean(point_frame['Lon'].values)))
    
    # measure the distance and heading between two points
    def between_points(self, start_point, end_point):
        start_coords = self.point_coords(start_point)
        end_coords = self.point_coords(end_point)
        distance = geopy.distance.distance(start_coords, end_coords).m
        lat_dist = geopy.distance.distance(start_coords, (end_coords[0], start_coords[1])).m
        lon_dist = geopy.distance.distance(start_coords, (start_coords[0], end_coords[1])).m
        theta = np.degrees(np.arctan(lon_dist/lat_dist))
        return(distance, theta)

    # output the points into an NPZ
    def output_points(self):
        """
        Outputs a list of points to an NPZ file

        Parameters
        ----------
        df : pd.DataFrame
            The DataFrame to output
        name : String
            The name of the dataset, for naming the output file

        Notes
        -----
        Saves data to ./npz/name_points.npz

        """
        mean_alt, std_alt, mean_az, std_az = [],[],[],[]
        
        for i in range(max(self.df['point'].values)):
            az = self.df[self.df['point'] == i]['az'].values
            el = self.df[self.df['point'] == i]['el'].values
            mean_alt.append(np.mean(el))
            mean_az.append(np.mean(az))
            std_alt.append(np.std(el))
            std_az.append(np.std(az))

        np.savez("./npz/"+self.name+"_points.npz", mean_alt=mean_alt, std_alt=std_alt, mean_az=mean_az, std_az=std_az)
        print("Saved NPZ")







#class CSVParser:
#TODO, will include load_columns
# may eventually extend to cover the functionality of DATCon




####################
###   LOAD_CSV   ###
####################


COL_LIST = ["RTKdata:Lat_P", "RTKdata:Lon_P", "RTKdata:Hmsl_P", "GPS:dateTimeStamp", 
"offsetTime", "Tick#", 'IMU_ATTI(0):velComposite', 'RTKdata:YAW', "RTKdata:pitch", 
"IMU_ATTI(0):roll", "IMU_ATTI(0):pitch", "IMU_ATTI(0):yaw", "IMU_ATTI(0):yaw360"]

# global forward declaration
rtk_p = pd.DataFrame(columns=["Lat", "Lon", "hmsl"])
 
def interpolate_time(df):
    if 'timestamp' in df.columns:
        print("Already interpolated")
        return 1

    # get a clock tick near sec_elapsed = 0
    in_flight  = df[df["sec_elapsed"] > 0.]
    first_time = in_flight["datetimestamp"].values[0]
    tick_row = 0
    # find the first in-flight clock tick
    while in_flight["datetimestamp"].values[tick_row] == first_time:
        tick_row += 1
        if tick_row >= len(in_flight['datetimestamp']): 
            tick_row -= 1
            break

    tick_num = in_flight["tick"].values[tick_row]
    # we are declaring this row to be t=0
    # shift sec_elapsed to fit this
    sec_offset = -1.*in_flight['sec_elapsed'].values[tick_row]
    tick_row = tick_row + (len(df["tick"].values) - len(in_flight["tick"].values))
    init_time = np.datetime64(df["datetimestamp"].values[tick_row])
    print("INIT TIME SET AT "+str(df['tick'].values[tick_row]))
    if (df["tick"].values[tick_row] != tick_num):
        print("alignment check failed")

    secs = np.add(df["sec_elapsed"].values, sec_offset)
    deltas = []
    i=0
    for row in secs:
        sec = floor(row)
        milli = int((row - sec)*1000)
        if(milli > 1000): print(milli)
        sec_delta = np.timedelta64(sec, 's')
        mil_delta = np.timedelta64(milli, 'ms')
        delta = sec_delta + mil_delta
        deltas.append(delta)
        i+=1

    moved_times = []
    for i in range(len(deltas)):
        moved_times.append(init_time + deltas[i])

    df = df.assign(timestamp = moved_times)
    return df


# loads the given columns of a single file
def load_columns(filename, columns=COL_LIST):
    FLY = pd.read_csv("./csv_files/"+filename+".csv",sep=",",header=0,usecols=columns)

    # rename columns, do some initial processing
    rtk_p = pd.DataFrame(columns=["Lat", "Lon", "hmsl"])
    rtk_p["Lat"] = FLY["RTKdata:Lat_P"]
    rtk_p["Lon"] = FLY["RTKdata:Lon_P"]
    rtk_p["hmsl"] = FLY["RTKdata:Hmsl_P"]
    rtk_p["sec_elapsed"] = FLY["offsetTime"]
    rtk_p["datetimestamp"] = FLY["GPS:dateTimeStamp"]
    rtk_p["tick"] = FLY["Tick#"]
    rtk_p["vel"] = FLY["IMU_ATTI(0):velComposite"]
    rtk_p["yaw_rtk"] = FLY["RTKdata:YAW"]
    rtk_p["pitch_rtk"] = FLY["RTKdata:pitch"]
    rtk_p["pitch"] = FLY["IMU_ATTI(0):pitch"]
    rtk_p["roll"] = FLY["IMU_ATTI(0):roll"]
    rtk_p["yaw"] = FLY["IMU_ATTI(0):yaw"]
    rtk_p["yaw_360"] = FLY["IMU_ATTI(0):yaw360"]
    rtk_p = rtk_p.where(rtk_p != 0.)
    rtk_p.dropna(inplace=True)
    if len(rtk_p.columns) == 0:
        print("File "+name+" is empty. YEET")
        return(pd.DataFrame(columns=[]))
    return(rtk_p)

# reads the given columns from a file and returns a pandas dataframe
def process_files(filenames, columns=COL_LIST, extra_col = []):
    """
    Processes raw CSVs into much smaller files.

    Accepts multiple files in chronological order, 
    and produces a single file containing all of the data. 

    This should be run once on each new input file.

    Parameters
    ----------
    filenames : list of str
        |  Filenames to load
        |  If you are using one file, it must be passed as ['file']
        |  Also supports multiple files: ['file1', 'file2', etc]
        |  Files should be listed in chronological order

    columns : list of str, optional
        |  Columns to read
        |  load_csv.py has a sane default list

    Returns
    -------
    pd.DataFrame
        A new DataFrame with your data


    """

    if len(extra_col) != 0:
        for col in extra_col:
            columns.append(col)

    frames = []
    for name in filenames:
        df = load_columns(name, columns)
        df = interpolate_time(df)
        if(df.empty): print("File "+name+" is empty. YEET")
        else: frames.append(df)
    print("STAPLING...")
    df = pd.DataFrame()
    for frame in frames:
        df = pd.concat([df, frame], ignore_index=True)

    if(len(frames) > 1):
        if((len(frames[0]['tick'].values)+len(frames[1]['tick'].values))==len(df['tick'].values)):
            print("STAPLING SUCCEEDED")
        for i in range(len(filenames) - 1):
            print("GAP # "+str(i))
            print(frames[i]['datetimestamp'].values[-1])
            print(frames[i+1]['datetimestamp'].values[0])
        else: print("ERROR: PLEASE CLEAR STAPLER JAM")

    # build a file name:
    name = ""
    for word in filenames:
        name = name + word
        name = name + "_"
    print(name)
    output_df(df, name)
    return df

# outputs the given dataframe as CSV and NPZ
def output_df(df, name):
    """
    |  Outputs your DataFrame to CSV and NPZ
    |  The CSV file will contain all columns
    |  The NPZ file will contain only timestamp, Lat, Lon, and hmsl

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to convert
    name : String
        The name of the data file, for naming output files

    Notes
    -----
    |  Saves NPZ to ./npz/nameDrone_Coordinates.npz
    |  Saves CSV to ./csv_out/nameprocessed.csv

    """
    # output a few columns of the stapled frame as a NPZ file
    np.savez("./npz/"+name+"Drone_Coordinates.npz",timestamp=df['timestamp'].values,Lat=df['Lat'].values,Lon=df['Lon'].values,hmsl=df['hmsl'].values, yaw=df['yaw'].values, vel=df['vel'].values)
    print("Saved NPZ")

    # output the whole stapled frame as a CSV file
    df.to_csv("./csv_out/"+name+"processed.csv")
    print("Saved CSV")


