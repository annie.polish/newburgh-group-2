# Annie Polish
# Summer 2019

# README: How to prepare data for this script
# Open DatCon (from datfile.net)
# Set Parsing Options to Engineered, then DatDefined
# Set the Sample Rate to 100Hz
# Set the output location to ./CSV_files (relative to this script)
# GO!
# In this file, set BASENAME to the name of your file
# Run this script. The plot will be saved to ./plots

import matplotlib as mpl
import matplotlib.cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 
import numpy as np
import pandas as pd
import math
from matplotlib.transforms import offset_copy
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
import geopy.distance

plt.rcParams.update({'font.size': 16})

########################
# CONSTANT DEFINITIONS #
########################
BASENAME = "FLY117" # filename to run on
BORDER = 0.00025 # sets the border around the dataset, in degrees of lat/long
PLOT_BG = cimgt.GoogleTiles(desired_tile_form='RGB', style='satellite')
COL_LIST = ["rtkdata:Lat_P", "rtkdata:Lon_P", "rtkdata:Hmsl_P", "GPS:dateTimeStamp", "offsetTime"]

# accepts a DataFrame with "Lat", "Lon", "hmsl"
# fills in the isMoving column with bools
# isMoving is a guess at whether the drone is currently moving
def fill_moving(df: pd.DataFrame, xy_thresh= 0.00000003, h_thresh=0.005, win = 50):
    print("top of fill_moving")
    latSer = pd.Series(df["Lat"].values, index=df.index)
    lonSer = pd.Series(df["Lon"].values, index=df.index)
    hmslSer= pd.Series(df["hmsl"].values,index=df.index)
    df = df.assign(std_lat = latSer.rolling(50, center=True).std())
    df = df.assign(std_lon = lonSer.rolling(50, center=True).std())
    df = df.assign(std_hmsl = hmslSer.rolling(50, center=True).std())
    df = df.assign(isMoving = np.logical_or(np.greater(df["std_lat"].values, xy_thresh), 
        np.greater(df["std_lon"].values, xy_thresh),
        np.greater(df["std_hmsl"].values, h_thresh)))
    
    print("bottom of fill_moving")
    return df

def fill_avMov(df: pd.DataFrame, win = 100):
    print("top of fill_avMov")
    latSer = pd.Series(df["Lat"].values, index=df.index)
    lonSer = pd.Series(df["Lon"].values, index=df.index)
    hmslSer= pd.Series(df["hmsl"].values,index=df.index)
    df = df.assign(avMovLat = latSer.rolling(50, center=True).mean())
    df = df.assign(avMovLon = lonSer.rolling(50, center=True).mean())
    df = df.assign(avMovHmsl = hmslSer.rolling(50, center=True).mean())

    print("bottom of fill_avMov")
    return df

# returns the statistics of the coordinates in a given time interval
def stationaryStats(df, startTime, endTime):
    df_slice = df[df["sec_elapsed"] > startTime]
    df_slice = df_slice[df_slice["sec_elapsed"] < endTime]

print("Hello imports")
print("starting file read")
FLY = pd.read_csv("./CSV_files/"+BASENAME+".csv", sep=",", header=0, usecols=COL_LIST)#, nrows=15000)
print("end of file read")


#RTKDATA
print("beginning initial processing")
rtk_p = pd.DataFrame(columns=["Lat", "Lon", "hmsl"])
rtk_p["Lat"] = FLY["rtkdata:Lat_P"]
rtk_p["Lon"] = FLY["rtkdata:Lon_P"]
rtk_p["hmsl"] = FLY["rtkdata:Hmsl_P"]
rtk_p["sec_elapsed"] = FLY["offsetTime"]
rtk_p = rtk_p.where(rtk_p != 0.)
rtk_p.dropna(thresh=2, inplace=True)
# arbitrarily trim the first and last ?? values
if(BASENAME == "FLY097"):
    rtk_p = rtk_p.iloc[11700:]
    rtk_p = rtk_p.iloc[:-1000]
else:
    rtk_p = rtk_p.iloc[30:]
    rtk_p = rtk_p.iloc[:-10]

print("completed initial processing")


rtk_p = fill_moving(rtk_p)
print(rtk_p)

rtk_p= fill_avMov(rtk_p)
print(rtk_p)

# retrieve the date
date = FLY["GPS:dateTimeStamp"].values[100]
date = date[:10]

# begin the plot
print("beginning plot render")
fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(1, 1, 1, projection=PLOT_BG.crs) # add a subplot in the coordinate system of the tiles
ax.set_extent([min(rtk_p["Lon"].values)-BORDER, max(rtk_p["Lon"].values)+BORDER, 
    min(rtk_p["Lat"].values)-BORDER, max(rtk_p["Lat"].values)+BORDER], crs=ccrs.Geodetic()) # set plot size
ax.add_image(PLOT_BG, 20, interpolation='spline36')
# plot all the data from the onboard gps
# plot the RTK gps data
rtk_mov = rtk_p[rtk_p["isMoving"] == True]
rtk_stop = rtk_p[rtk_p["isMoving"] == False]
#plt.plot(rtk_p["Lon"].values, rtk_p["Lat"].values, 'c-', transform=ccrs.Geodetic())
plt.plot(rtk_p["avMovLon"].values, rtk_p["avMovLat"].values, 'k:', transform=ccrs.Geodetic())
plt.scatter(rtk_mov["Lon"].values, rtk_mov["Lat"].values, c='c', transform=ccrs.Geodetic())
plt.scatter(rtk_stop["Lon"].values,rtk_stop["Lat"].values,c='m', transform=ccrs.Geodetic())
# label stuff and save
plt.xlabel("Longitude")
plt.ylabel("Latitude")
plt.title("RTK (cyan) vs onboard GPS (black) - "+BASENAME+" "+str(date))
plt.grid()
fig.canvas.draw()
plt.tight_layout()
plt.savefig("./plots/"+BASENAME+".png")
print("saved plot")
plt.show()

fig = plt.figure(figsize=(10,10))
plt.title("Longitude vs Elapsed Time (seconds)")
plt.ylim(min(rtk_p["Lon"].values), max(rtk_p["Lon"].values))
plt.scatter(rtk_p["sec_elapsed"].values, rtk_p["Lon"].values)
plt.grid()
plt.savefig("./plots"+BASENAME+"_lon.png")
plt.show()

fig = plt.figure(figsize=(10,10))
plt.title("Latitude vs Elapsed Time (seconds)")
plt.ylim(min(rtk_p["Lat"].values), max(rtk_p["Lat"].values))
plt.scatter(rtk_p["sec_elapsed"].values, rtk_p["Lat"].values)
plt.grid()
plt.savefig("./plots"+BASENAME+"_lat.png")
plt.show()


fig = plt.figure(figsize=(10,10))
plt.title("Height vs Elapsed Time (seconds)")
plt.ylim(min(rtk_p["hmsl"].values), max(rtk_p["hmsl"].values))
plt.scatter(rtk_p["sec_elapsed"].values, rtk_p["hmsl"].values)
plt.grid()
plt.savefig("./plots"+BASENAME+"_hmsl.png")
plt.show()


fig = plt.figure()
ax = fig.add_subplot(1,1,1,projection='3d')
ax.plot(rtk_p['Lon'].values, rtk_p['Lat'].values, rtk_p['hmsl'].values)
plt.savefig("./plots/3d"+BASENAME+".png")
plt.show()



print("EOF")
