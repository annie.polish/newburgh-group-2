
import matplotlib as mpl
import matplotlib.cm
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from mpl_toolkits.mplot3d import Axes3D 
import numpy as np
import pandas as pd
import pymap3d as pm
from math import *
from datetime import datetime
from matplotlib.transforms import offset_copy
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import geopy.distance
from scalebar import scale_bar #requires the existance of scalebar.py

import class_refactor as drone


NAME = 'FLY215'
df = pd.read_csv("./csv_out/"+NAME+"_processed.csv", sep=',', header=0)

df = df.assign(timestamp = pd.to_datetime(df['timestamp'].values))
df = df.where(df != 0.) 
df.dropna(thresh=2, inplace=True)

data = drone.DroneData(df, NAME)
print(data.df.columns)
data.interpolate_time()
data.fill_moving()
print("added columns")
print(data.df.columns)

data.plot('Lat')
data.plot('hmsl')
data.plot_points()
