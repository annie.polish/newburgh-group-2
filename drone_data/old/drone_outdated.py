######################################
#           Annie Polish             #
#           Summer 2019              #
#  Drone data processing functions   #
######################################


import matplotlib as mpl
import matplotlib.cm
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from mpl_toolkits.mplot3d import Axes3D 
import numpy as np
import pandas as pd
import pymap3d as pm
from math import *
from datetime import datetime
from matplotlib.transforms import offset_copy
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import geopy.distance
from scalebar import scale_bar #requires the existance of scalebar.py


####################
###   LOAD_CSV   ###
####################

COL_LIST = ["RTKdata:Lat_P", "RTKdata:Lon_P", "RTKdata:Hmsl_P", "GPS:dateTimeStamp", "offsetTime", "Tick#", 'IMU_ATTI(0):velComposite', 'RTKdata:YAW', 'RC:Aileron', 'RC:Elevator', 'RC:Rudder', 'RC:Throttle']

# global forward declaration
rtk_p = pd.DataFrame(columns=["Lat", "Lon", "hmsl"])
 
# loads the given columns of a single file
def load_columns(filename, columns=COL_LIST):
    FLY = pd.read_csv("./csv_files/"+filename+".csv",sep=",",header=0,usecols=columns)

    # rename columns, do some initial processing
    rtk_p = pd.DataFrame(columns=["Lat", "Lon", "hmsl"])
    rtk_p["Lat"] = FLY["RTKdata:Lat_P"]
    rtk_p["Lon"] = FLY["RTKdata:Lon_P"]
    rtk_p["hmsl"] = FLY["RTKdata:Hmsl_P"]
    rtk_p["sec_elapsed"] = FLY["offsetTime"]
    rtk_p["datetimestamp"] = FLY["GPS:dateTimeStamp"]
    rtk_p["tick"] = FLY["Tick#"]
    rtk_p["vel"] = FLY["IMU_ATTI(0):velComposite"]
    rtk_p["yaw"] = FLY["RTKdata:YAW"]
    rtk_p = rtk_p.where(rtk_p != 0.)
    rtk_p.dropna(inplace=True)
    rtk_p["control"] = (np.logical_or(np.logical_or(FLY['RC:Aileron'], FLY['RC:Elevator']), 
            np.logical_or(FLY['RC:Rudder'], FLY['RC:Throttle']))).astype(int)
    if len(rtk_p.columns) == 0:
        print("File "+name+" is empty. YEET")
        return(pd.DataFrame(columns=[]))
    return(rtk_p)

# interpolate milliseconds and UTC timestamps
def interpolate_time(df):
    # get a clock tick near sec_elapsed = 0
    in_flight  = df[df["sec_elapsed"] > 0.]
    if in_flight.empty:
        print("FILE CONTAINS NO FLIGHT DATA")
        return(in_flight)
    first_time = in_flight["datetimestamp"].values[0]
    tick_row = 0
    # find the first in-flight clock tick
    while in_flight["datetimestamp"].values[tick_row] == first_time:
        tick_row += 1
        if tick_row >= len(in_flight['datetimestamp']): 
            tick_row -= 1
            break

    tick_num = in_flight["tick"].values[tick_row]

    print(tick_row)
       
    # we are declaring this row to be t=0
    # shift sec_elapsed to fit this
    sec_offset = -1.*in_flight['sec_elapsed'].values[tick_row]
    print(sec_offset)

    tick_row = tick_row + (len(df["tick"].values) - len(in_flight["tick"].values))
    init_time = np.datetime64(df["datetimestamp"].values[tick_row])
    print(init_time)
    print("INIT TIME SET AT "+str(df['tick'].values[tick_row]))
    if (df["tick"].values[tick_row] != tick_num):
        print("alignment check failed")
    
    secs = np.add(df["sec_elapsed"].values, sec_offset)
    deltas = []
    i=0
    for row in secs:
        sec = floor(row)
        milli = int((row - sec)*1000)
        if(milli > 1000): print(milli)
        sec_delta = np.timedelta64(sec, 's')
        mil_delta = np.timedelta64(milli, 'ms')
        delta = sec_delta + mil_delta
        deltas.append(delta)
        i+=1
    
    moved_times = []
    for i in range(len(deltas)):
        moved_times.append(init_time + deltas[i])
    
    df = df.assign(timestamp = moved_times)
    return(df)

# reads the given columns from a file and returns a pandas dataframe
def process_files(filenames, columns=COL_LIST, extra_col = []):
    """
    Processes raw CSVs into much smaller files.

    Accepts multiple files in chronological order, 
    and produces a single file containing all of the data. 

    This should be run once on each new input file.

    Parameters
    ----------
    filenames : list of str
        |  Filenames to load
        |  If you are using one file, it must be passed as ['file']
        |  Also supports multiple files: ['file1', 'file2', etc]
        |  Files should be listed in chronological order

    columns : list of str, optional
        |  Columns to read
        |  load_csv.py has a sane default list

    Returns
    -------
    pd.DataFrame
        A new DataFrame with your data


    """

    if len(extra_col) != 0:
        for col in extra_col:
            columns.append(col)

    frames = []
    for name in filenames:
        df = load_columns(name, columns)
        df = interpolate_time(df)
        if(df.empty): print("File "+name+" is empty. YEET")
        else: frames.append(df)
    print("STAPLING...")
    df = pd.DataFrame()
    for frame in frames:
        df = pd.concat([df, frame], ignore_index=True)

    if(len(frames) > 1):
        if((len(frames[0]['tick'].values)+len(frames[1]['tick'].values))==len(df['tick'].values)):
            print("STAPLING SUCCEEDED")
        for i in range(len(filenames) - 1):
            print("GAP # "+str(i))
            print(frames[i]['datetimestamp'].values[-1])
            print(frames[i+1]['datetimestamp'].values[0])
        else: print("ERROR: PLEASE CLEAR STAPLER JAM")

    # build a file name:
    name = ""
    for word in filenames:
        name = name + word
        name = name + "_"
    print(name)
    output_df(df, name)
    return df

# outputs the given dataframe as CSV and NPZ
def output_df(df, name):
    """
    |  Outputs your DataFrame to CSV and NPZ
    |  The CSV file will contain all columns
    |  The NPZ file will contain only timestamp, Lat, Lon, and hmsl

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to convert
    name : String
        The name of the data file, for naming output files

    Notes
    -----
    |  Saves NPZ to ./npz/nameDrone_Coordinates.npz
    |  Saves CSV to ./csv_out/nameprocessed.csv

    """
    # output a few columns of the stapled frame as a NPZ file
    np.savez("./npz/"+name+"Drone_Coordinates.npz",timestamp=df['timestamp'].values,Lat=df['Lat'].values,Lon=df['Lon'].values,hmsl=df['hmsl'].values, yaw=df['yaw'].values, vel=df['vel'].values, control=df['control'].values)
    print("Saved NPZ")

    # output the whole stapled frame as a CSV file
    df.to_csv("./csv_out/"+name+"processed.csv")
    print("Saved CSV")


###################
###   COLUMNS   ###
###################

# isMoving is a guess at whether the drone is currently moving
def fill_moving(df: pd.DataFrame, method='av', vel_thresh=1.0, xy_thresh= 0.00000003, h_thresh=0.005, win = 50):
    """
    Fills a column with a bool indicating if the drone is moving

    This adds the following columns:
        * std_lat : float, latitude standard deviation
        * std_lon : float, longitude standard deviation
        * std_hmsl : float, height standard deviation
        * isMoving : bool


    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to examine and add a column
    xy_thresh : float, optional
        In units of degrees of lat/lon.
        If coordinates exit this radius, they are moving
    h_thresh : float, optional
        In units of meters. 
        If height differs from the average by this much, the point is moving
    win : int, optional
        Number of points for the rolling average window


    Returns
    -------
    pd.DataFrame
        The DataFrame you passed in, with one extra column

    """

    latSer = pd.Series(df["Lat"].values, index=df.index)
    lonSer = pd.Series(df["Lon"].values, index=df.index)
    hmslSer= pd.Series(df["hmsl"].values,index=df.index)
    df = df.assign(std_lat = latSer.rolling(50, center=True).std())
    df = df.assign(std_lon = lonSer.rolling(50, center=True).std())
    df = df.assign(std_hmsl = hmslSer.rolling(50, center=True).std())
    
    if method == 'av':
        df = df.assign(isMoving = np.logical_or(np.greater(df["std_lat"].values,xy_thresh), 
            np.greater(df["std_lon"].values, xy_thresh),
            np.greater(df["std_hmsl"].values, h_thresh)))
    elif method == 'vel': 
        df = df.assign(isMoving = np.greater(df['vel'].values, vel_thresh))
    else: print('INVALID MOVING METHOD')
    return df

# fill in moving averages
def fill_avMov(df: pd.DataFrame, win = 20):
    """
    Fills a column with a moving average in each coordinate

    This adds the following columns:
        * avMovLat : float, latitude moving average
        * avMovLon : float, longitude moving average
        * avMovHmsl : float, height moving average

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to analyze
    win : int, optional
        The window for the rolling average, in number of points

    Returns
    -------
    pd.DataFrame
        The DataFrame you passed in, with some extra columns

    """


    latSer = pd.Series(df["Lat"].values, index=df.index)
    lonSer = pd.Series(df["Lon"].values, index=df.index)
    hmslSer= pd.Series(df["hmsl"].values,index=df.index)
    df = df.assign(avMovLat = latSer.rolling(win, center=True).mean())
    df = df.assign(avMovLon = lonSer.rolling(win, center=True).mean())
    df = df.assign(avMovHmsl = hmslSer.rolling(win, center=True).mean())
    return df

# slice up the dataframe
def chop_time(df, startTime, endTime):
    """
    Function to take a time slice of a DataFrame

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to slice
    startTime : np.datetime64
        Time where your new DataFrame begins
    endTime : np.datetime64
        Time where your new DataFrame ends

    Returns
    -------
    pd.DataFrame
        The slice of DataFrame in the given time interval

    """

    df_slice = df[df["timestamp"] > startTime]
    df_slice = df_slice[df_slice["timestamp"] < endTime]
    return(df_slice)

# uses pymap3d for coordinate transformation
def make_ENU(df, startTime=0, endTime=0, lat_origin=0, lon_origin=0, height_origin=0):
    """
    Moves the DataFrame into a local coordinate system

    Requires an origin point of the coordinate system.
    This function supports two ways of specifying the origin:
    1. Using startTime and endTime to give a time interval over which you average
    2. Directly providing a set of coordinates in lat, lon, hmsl

    This adds the following columns:
     * east : float, UNIT TBD
     * north : float, UNIT TBD
     * up : float, UNIT TBD

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to analyze
    startTime : np.datetime64, optional
        Beginning of averaging interval for determining the origin
    endTime : np.datetime64, optional
        End of averaging interval for determining the origin
    lat_origin : float, optional
        Latitude of the origin point
    lon_origin : float, optional
        Longitude of the origin point
    height_origin : float, optional
        Height above MSL of the origin point

    Returns
    -------
    pd.DataFrame
        The DataFrame you passed in, with some extra columns 


    """
    if(startTime == 0):
        startTime = df['timestamp'].values[0]
    if(endTime == 0):
        endTime = df['timestamp'].values[-1]
    # find the origin of our transformed coordinate system
    # z first:
    if (height_origin == 0):
        height_origin = np.mean(df[df["sec_elapsed"] < 0]["hmsl"].values)
        if (height_origin == 0 or np.isnan(height_origin)):
            height_origin = np.min(df['hmsl'].values)
        print(height_origin)
    
    # lat+long:
    df_slice = chop_time(df, startTime, endTime)
    if (lat_origin == 0): lat_origin = np.mean(df_slice["Lat"].values)
    if (lon_origin == 0): lon_origin = np.mean(df_slice["Lon"].values)
    print(lat_origin)
    print(lon_origin)
    
    east, north, up = pm.geodetic2enu(df["Lat"].values, df["Lon"].values, df["hmsl"].values,
                                      lat_origin, lon_origin, height_origin)
    
    df = df.assign(east = east)
    df = df.assign(north = north)
    df = df.assign(up = up)
    return(df)


def make_az_el(df, startTime=0, endTime=0, lat_origin=0, lon_origin=0, height_origin=0):
    """
    Moves the DataFrame into a local coordinate system

    Requires an origin point of the coordinate system.
    This function supports two ways of specifying the origin:
    1. Using startTime and endTime to give a time interval over which you average
    2. Directly providing a set of coordinates in lat, lon, hmsl

    This adds the following columns:
     * az : float, degrees
     * el : float, degrees
     * ran : float, UNIT TBD

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to analyze
    startTime : np.datetime64, optional
        Beginning of averaging interval for determining the origin
    endTime : np.datetime64, optional
        End of averaging interval for determining the origin
    lat_origin : float, optional
        Latitude of the origin point
    lon_origin : float, optional
        Longitude of the origin point
    height_origin : float, optional
        Height above MSL of the origin point

    Returns
    -------
    pd.DataFrame
        The DataFrame you passed in, with some extra columns 


    """
    if(startTime == 0):
        startTime = df['timestamp'].values[0]
    if(endTime == 0):
        endTime = df['timestamp'].values[-1]
    # find the origin of our transformed coordinate system
    # z first:
    if (height_origin == 0):
        height_origin = np.mean(df[df["sec_elapsed"] < 0]["hmsl"].values)
        if (height_origin == 0 or np.isnan(height_origin)):
            height_origin = np.min(df['hmsl'].values)
        print(height_origin)
    
    # lat+long:
    df_slice = chop_time(df, startTime, endTime)
    if (lat_origin == 0): lat_origin = np.mean(df_slice["Lat"].values)
    if (lon_origin == 0): lon_origin = np.mean(df_slice["Lon"].values)
    print(lat_origin)
    print(lon_origin)
    
    az, el, ran = pm.geodetic2aer(df["Lat"].values, df["Lon"].values, df["hmsl"].values,
                       lat_origin, lon_origin, height_origin)
    
    df = df.assign(az = az)
    df = df.assign(el = el)
    df = df.assign(ran = ran)
    return(df)


####################
###   PLOTTING   ###
####################

# defines some global properties of plots
BORDER = 0.00003 # sets the border around the dataset, in degrees of lat/long
MAPTYPE = 'mapbox' #stamen, mapbox, or google (BYO MapBox key)

if MAPTYPE == 'mapbox':
    # your mapbox.com API token
    MAPBOX_KEY = "pk.eyJ1IjoiYW5uaWVwb2xpc2giLCJhIjoiY2p5b3BwdXl3MTdhdzNjdDRjbGw5MWJ6ciJ9.01NjskBuc2SQcm5QjbyLwA"
    MAPBOX_STYLE = "cjypy1k7x0ru71cjva7cs5iwz"
    MAPBOX_USERNAME = "anniepolish"
    PLOT_BG = cimgt.MapboxStyleTiles(MAPBOX_KEY, MAPBOX_USERNAME, MAPBOX_STYLE)
elif MAPTYPE == 'stamen':
    PLOT_BG = cimgt.Stamen('terrain')
elif MAPTYPE == 'google':
    PLOT_BG = cimgt.GoogleWTS()

date_format = dates.DateFormatter("%H:%M:%S")

# interface for changing the plot border
def set_border(border):
    """
    Changes the border of plots
    
    Parameters
    ----------
    border : float
        Size of the border in degrees of lat/lon.
        Reasonable sizes are 0.00001 to 0.00009

    """

    if (border < 0.1 and border >= 0):
        BORDER = border
    else:
        print("Error: bad border value")
        return -1
    return 0

# plot the flight path over satellite imagery
def plot_satellite(df, name, figsize=(10, 10), color='c', avMov=True):
    """
    Plots the drone's flight path over a satellite map

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for labeling and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    color : String, optional
        Passed to matplotlib. Defaults to 'c'
    avMov : bool, optional
        Moving average is used if True. 
        Raw coordinates are used if False. 
        Defaults to True

    Notes
    -----
    Saves plot to ./plots/name.png

    """
    # make sure the expected columns exist
    if(not "Lat" in df.columns or not "Lon" in df.columns):
        print("Error: bad dataframe - Lat/Lon not found")
        return -1
    if(not "timestamp" in df.columns):
        print("Error: bad dataframe - timestamp not found")
        return -1
    if(avMov and (not "avMovLat" in df.columns or not "avMovLon" in df.columns)):
        print("Error: bad dataframe - avMov not found")
        print("Specify avMov=False if you do not have a moving average")
        return -1
   
    # find the date for the plot title
    date = str(df["timestamp"].values[0])[:10]

    # begin the plot
    plt.rcParams.update({'font.size': 16})
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(1, 1, 1, projection=PLOT_BG.crs) # add a subplot in the coordinate system of the tiles
    ax.set_extent([min(df["Lon"].values)-BORDER, max(df["Lon"].values)+BORDER, 
        min(df["Lat"].values)-BORDER, max(df["Lat"].values)+BORDER], crs=ccrs.Geodetic()) # set plot size
    ax.add_image(PLOT_BG, 20, interpolation='spline36') # go get some map tiles
    # plot the RTK gps data
    if(avMov):
        plt.plot(df["avMovLon"].values, df["avMovLat"].values, color, linewidth=2, transform=ccrs.Geodetic(), label="RTK")
    else:
        plt.plot(df["Lon"].values, df["Lat"].values, color, linewidth=2, transform=ccrs.Geodetic, label="RTK")
    # label stuff and save
    plt.xlabel("Longitude")
    plt.ylabel("Latitude")
    plt.title("RTK data - "+name+" - "+str(date))
    fig.canvas.draw()
    plt.tight_layout()
    plt.savefig("./plots/"+name+".png")
    plt.show()

# plot points over satellite imagery
def plot_points(df, name, figsize=(10, 10), legend=True, errorbars=False, tiles=True):
    """
    Plots a list of points over a satellite map

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for labeling and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    legend : bool, optional
        Turns the plot legend on or off. Defaults to True
    errorbars : bool, optional
        Turns error bars on or off. Defaults to False

    Notes
    -----
    Saves plot to ./plots/points_name.png


    """
    if(not 'point' in df.columns):
        print("Error: no points")
        return -1
    
    # find the date for the plot title
    date = str(df["timestamp"].values[0])[:10]

    #plot!
    fig = plt.figure(figsize=figsize)
    cm = plt.get_cmap('tab20')
    ax = fig.add_subplot(1,1,1, projection=PLOT_BG.crs)
    ax.set_prop_cycle(color=[cm(1.*i/20) for i in range(20)])
    ax.set_extent([min(df["Lon"].values)-BORDER, max(df["Lon"].values)+BORDER, 
        min(df["Lat"].values)-BORDER, max(df["Lat"].values)+BORDER], crs=ccrs.Geodetic())
    if tiles:
        ax.add_image(PLOT_BG, 20, interpolation='spline36') # go get some map tiles
    # plot each point
    for i in range(max(df['point'].values)):
        lat = df[df['point'] == i]['Lat'].values
        lon = df[df['point'] == i]['Lon'].values
        plt.plot(lon, lat, label=str(i)+"-"+str(df[df['point']==i]['timestamp'].values[0])[11:19], transform=ccrs.Geodetic(), linewidth=3.0)
        if(errorbars): plt.errorbar(np.mean(lat), np.mean(lon), xerr=np.std(lat), yerr=np.std(lon), color = 'm', zorder=10)
    # label stuff and save
    plt.xlabel("Longitude")
    plt.ylabel("Latitude")
    if(legend): plt.legend()
    plt.title("Stationary points - "+name+" "+str(date))
    plt.grid()
    fig.canvas.draw()
    plt.tight_layout()
    plt.savefig("./plots/points_"+name+".png")
    print("saved plot")
    plt.show()

# plot latitude vs time
def plot_latitude(df, name, figsize=(10,10), color='C0'):
    """
    Plots latitude vs time

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for naming and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    color : String, optional
        Passed to matplotlib. Defaults to 'C0'

    Notes
    -----
    Saves plot to ./plots/name_lat.png
    """
    plt.rcParams.update({'font.size': 16})
    fig = plt.figure(figsize=figsize)
    ax = plt.gca()
    ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)
    ax.yaxis.set_minor_formatter(LATITUDE_FORMATTER)
    ax.xaxis.set_major_formatter(date_format)
    ax.xaxis_date()
    plt.title("Latitude vs UTC Time")
    plt.ylim(min(df['Lat'].values), max(df['Lat'].values))
    plt.plot(df['timestamp'].values, df['Lat'].values, color)
    plt.grid()
    plt.xlim(min(df['timestamp'].values),max(df["timestamp"].values))
    plt.xticks(rotation=50)
    #plt.tight_layout()
    plt.savefig("./plots/"+name+"_lat.png")
    plt.show()

# plot longitude vs time
def plot_longitude(df, name, figsize=(10,10), color='C0'):
    """
    Plots longitude vs time

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for naming and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    color : String, optional
        Passed to matplotlib. Defaults to 'C0'

    Notes
    -----
    Saves plot to ./plots/name_lon.png
    """

    plt.rcParams.update({'font.size': 16})
    fig = plt.figure(figsize=figsize)
    plt.title("Longitude vs UTC Time")
    plt.ylim(min(df['Lon'].values), max(df['Lon'].values))
    plt.plot(df['timestamp'].values, df['Lon'].values, color)
    plt.grid()
    ax = plt.gca()
    ax.yaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_minor_formatter(LONGITUDE_FORMATTER)
    ax.xaxis.set_major_formatter(date_format)
    plt.xlim(min(df['timestamp'].values),max(df["timestamp"].values))
    plt.xticks(rotation=50)
    #plt.tight_layout()
    plt.savefig("./plots/"+name+"_lon.png")
    plt.show()

# plot height vs time
def plot_height(df, name, figsize=(10,10), color='C0'):
    """
    Plots height vs time

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for naming and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    color : String, optional
        Passed to matplotlib. Defaults to 'C0'

    Notes
    -----
    Saves plot to ./plots/name_hmsl.png
    """

    fig = plt.figure(figsize=figsize)
    plt.title("Height vs Elapsed Time")
    plt.ylim(min(df["hmsl"].values), max(df["hmsl"].values))
    plt.plot(df["timestamp"].values, df["hmsl"].values, color)
    plt.grid()
    plt.gca().xaxis.set_major_formatter(date_format)
    plt.xticks(rotation=50)
    plt.savefig("./plots/"+name+"_hmsl.png")
    plt.show()

# plot moving and not moving for point tagging
def plot_moving(df, name, figsize=(10,10), view="Lon"):
    """
    Plots a coordinate vs time, with different colors when moving or not moving

    This is used to manually tag points.
    It can only be run after filling the isMoving column

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for naming and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    view : String, optional
        Column of the dataframe to plot. 
        Defaults to 'Lon'. 
        Other useful options are 'Lat' and 'hmsl'

    Notes
    -----
    This one does not save plots


    """
    if(not "isMoving" in df.columns):
        print("Error: no movement column")
        return -1
    if(not view in df.columns):
        print("Error: invalid view")
        return -1
    rtk_mov = df[df["isMoving"] == True]
    rtk_stop = df[df["isMoving"] == False]

    fig = plt.figure(figsize=figsize)
    plt.title(view+" vs Seconds Elapsed")
    plt.ylim(min(df[view].values), max(df[view].values))
    plt.scatter(rtk_mov['timestamp'].values, rtk_mov[view].values, s=2, color='c')
    plt.scatter(rtk_stop['timestamp'].values, rtk_stop[view].values, s=2, color='m')
    plt.grid()
    ax = plt.gca()
    ax.yaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_minor_formatter(LONGITUDE_FORMATTER)
    ax.xaxis.set_major_formatter(date_format)
    plt.xlim(min(df['timestamp'].values),max(df["timestamp"].values))
    plt.tight_layout()
    plt.show()

def plot_vel(df, name, figsize=(10,10)):

    fig = plt.figure(figsize=figsize)
    plt.title("Velocity vs Seconds Elapsed")
    plt.plot(df['timestamp'].values, df['vel'].values)
    plt.grid()
    ax=plt.gca()
    ax.xaxis.set_major_formatter(date_format)
    plt.xlim(min(df['timestamp'].values),max(df["timestamp"].values))
    plt.tight_layout()
    plt.show()

# General plotter function
# TODO switch case for axis formating, should support multiple axes

# Plot rotation
def plot_yaw(df, name, figsize=(10,10)):

    fig = plt.figure(figsize=figsize)
    plt.title("Yaw vs Seconds Elapsed")
    plt.plot(df['timestamp'].values, df['yaw'].values)
    plt.grid()
    ax=plt.gca()
    ax.xaxis.set_major_formatter(date_format)
    plt.xlim(min(df['timestamp'].values),max(df["timestamp"].values))
    plt.tight_layout()
    plt.show()


def plot_control(df, name, figsize=(10,10), view="Lon"):
    """
    Plots a coordinate vs time, with different colors when moving or not moving

    This is used to manually tag points.
    It can only be run after filling the isMoving column

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to plot
    name : String
        The name of the data file, for naming and saving plots
    figsize : tuple, optional
        Passed to matplotlib. Defaults to (10,10)
    view : String, optional
        Column of the dataframe to plot. 
        Defaults to 'Lon'. 
        Other useful options are 'Lat' and 'hmsl'

    Notes
    -----
    This one does not save plots


    """
    if(not "isMoving" in df.columns):
        print("Error: no movement column")
        return -1
    if(not view in df.columns):
        print("Error: invalid view")
        return -1
    rtk_mov = df[df["control"] == 1.]
    rtk_stop = df[df["control"] != 1.]

    fig = plt.figure(figsize=figsize)
    plt.title(view+" vs Seconds Elapsed")
    plt.ylim(min(df[view].values), max(df[view].values))
    plt.scatter(rtk_mov['timestamp'].values, rtk_mov[view].values, s=2, color='c')
    plt.scatter(rtk_stop['timestamp'].values, rtk_stop[view].values, s=2, color='m')
    plt.grid()
    ax = plt.gca()
    ax.yaxis.set_major_formatter(LONGITUDE_FORMATTER)
    ax.yaxis.set_minor_formatter(LONGITUDE_FORMATTER)
    ax.xaxis.set_major_formatter(date_format)
    plt.xlim(min(df['timestamp'].values),max(df["timestamp"].values))
    plt.tight_layout()
    plt.show()



##############
### POINTS ###
##############

# returns the statistics of the coordinates in a given time interval
def interval_stats(df, startTime, endTime, avg=False):
    """
    |  Performs various statistics on the given time interval
    |  Converts these statistics from lat/lon to meters

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to examine
    startTime : np.datetime64
        Start of measurement interval
    endTime : np.datetime64
        End of measurement interval
    avg : bool, optional
        Use moving average? Defaults to false

    Returns
    -------
    tuple
        Contains: (lat_distance, lon_distance, hmsl_dev, xy_distance)

    """


    print('hello stationary stats')
    # slice off part of the dataframe
    df_slice = df[df["timestamp"] > startTime]
    df_slice = df_slice[df_slice["timestamp"] < endTime]
    if (avg):
        lat_arr = df_slice["avMovLat"].values
        lon_arr = df_slice["avMovLon"].values
        hmsl_arr= df_slice["avMovHmsl"].values
    else:
        lat_arr  = df_slice["Lat"].values
        lon_arr  = df_slice["Lon"].values
        hmsl_arr = df_slice["hmsl"].values
    # find max and min of each coordinate, and the distance that represents
    max_lat  = np.max(lat_arr)
    min_lat  = np.min(lat_arr)
    max_lon  = np.max(lon_arr)
    min_lon  = np.min(lon_arr)
    lat_dist = geopy.distance.distance((min_lat, lon_arr[0]), (max_lat, lon_arr[0])).m
    lon_dist = geopy.distance.distance((lat_arr[0], min_lon), (lat_arr[0], max_lon)).m
    print("Latitude spread: "+str(lat_dist))
    print("Longitude spread: "+str(lon_dist))
    print("Diagonal distance: "+str(sqrt(lat_dist**2 + lon_dist**2)))
    print("Double check: "+str(geopy.distance.distance((min_lat, min_lon),(max_lat, max_lon)).m))
    # find the standard deviation of each coordinate
    lat_dev  = np.std(lat_arr)
    lon_dev  = np.std(lon_arr)
    hmsl_dev = np.std(hmsl_arr)
    # convert stddev in degrees to stddev in meters
    startCoords = (lat_arr[0], lon_arr[0])
    latCoords = (lat_arr[0]+lat_dev, lon_arr[0])
    lonCoords = (lat_arr[0], lon_arr[0]+lon_dev)
    endCoords = (lat_arr[0]+lat_dev, lon_arr[0]+lon_dev)
    lat_dist = geopy.distance.distance(startCoords, latCoords).m
    lon_dist = geopy.distance.distance(startCoords, lonCoords).m
    xy_dist = geopy.distance.distance(startCoords, endCoords).m
    # print and return
    print("Latitude standard deviation: "+str(lat_dist)+" meters")
    print("Longitude standard deviation: "+str(lon_dist)+" meters")
    print("Height standard deviation: "+str(hmsl_dev)+" meters")
    print("Diagonal lat-lon stddev: "+str(xy_dist)+" meters")
    return (lat_dist, lon_dist, hmsl_dev, xy_dist)

# returns the statistics of the coordinates in a given time interval
def point_stats(df, point, avg=False):
    """
    |  Performs various statistics on the given point
    |  Converts these statistics from lat/lon to meters

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to examine
    point : int
        Which point is being examined
    avg : bool, optional
        Use moving average? Defaults to false

    Returns
    -------
    tuple
        Contains: (lat_distance, lon_distance, hmsl_dev, xy_distance)

    """


    print('hello stationary stats')
    # slice off part of the dataframe
    df_slice = df[df["point"] == point]
    if (avg):
        lat_arr = df_slice["avMovLat"].values
        lon_arr = df_slice["avMovLon"].values
        hmsl_arr= df_slice["avMovHmsl"].values
    else:
        lat_arr  = df_slice["Lat"].values
        lon_arr  = df_slice["Lon"].values
        hmsl_arr = df_slice["hmsl"].values
    # find max and min of each coordinate, and the distance that represents
    max_lat  = np.max(lat_arr)
    min_lat  = np.min(lat_arr)
    max_lon  = np.max(lon_arr)
    min_lon  = np.min(lon_arr)
    lat_dist = geopy.distance.distance((min_lat, lon_arr[0]), (max_lat, lon_arr[0])).m
    lon_dist = geopy.distance.distance((lat_arr[0], min_lon), (lat_arr[0], max_lon)).m
    print("Latitude spread: "+str(lat_dist))
    print("Longitude spread: "+str(lon_dist))
    print("Diagonal distance: "+str(sqrt(lat_dist**2 + lon_dist**2)))
    print("Double check: "+str(geopy.distance.distance((min_lat, min_lon),(max_lat, max_lon)).m))
    # find the standard deviation of each coordinate
    lat_dev  = np.std(lat_arr)
    lon_dev  = np.std(lon_arr)
    hmsl_dev = np.std(hmsl_arr)
    # convert stddev in degrees to stddev in meters
    startCoords = (lat_arr[0], lon_arr[0])
    latCoords = (lat_arr[0]+lat_dev, lon_arr[0])
    lonCoords = (lat_arr[0], lon_arr[0]+lon_dev)
    endCoords = (lat_arr[0]+lat_dev, lon_arr[0]+lon_dev)
    lat_dist = geopy.distance.distance(startCoords, latCoords).m
    lon_dist = geopy.distance.distance(startCoords, lonCoords).m
    xy_dist = geopy.distance.distance(startCoords, endCoords).m
    # print and return
    print("Latitude standard deviation: "+str(lat_dist)+" meters")
    print("Longitude standard deviation: "+str(lon_dist)+" meters")
    print("Height standard deviation: "+str(hmsl_dev)+" meters")
    print("Diagonal lat-lon stddev: "+str(xy_dist)+" meters")
    return (lat_dist, lon_dist, hmsl_dev, xy_dist)

# measure the distance between two second markers
def dist_between_times(df, startTime, endTime):
    """
    Reports the distance between two timestamps

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to examine
    startTime : np.datetime64
        The time at which you measure your first point
    endTime : np.datetime64
        The time at which you measure your second point

    Returns
    -------
    float
        The distance between the two points, in meters

    """
    df_slice = df[df["timestamp"] > startTime]
    df_slice = df_slice[df_slice["timestamp"] < endTime]
    start_lat = df_slice.iloc[0]["Lat"]
    start_lon = df_slice.iloc[0]["Lon"]
    end_lat = df_slice.iloc[-1]["Lat"]
    end_lon = df_slice.iloc[-1]["Lon"]
    dist = geopy.distance.distance((start_lat, start_lon), (end_lat, end_lon)).m
    print(start_lat)
    print(start_lon)
    print(end_lat)
    print(end_lon)
    print("distance: "+str(dist))
    return(dist)

# mark a point by flipping values in the point column
def mark_point(df, timestamp, thresh = 0.2):
    """
    Records a point into the DataFrame

    Starts from the given timestamp, and expands the range out. 
    Range is expanded until the coordinates differ by more than the thresh. 
    The whole range then has its 'point' column updated

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to examine and modify
    timestamp : np.datetime64
        The timestamp to use as the origin of the point
    thresh : float, optional
        Threshold bounding a point. Defaults to 0.2. 
        Increase this value to make points more error-tolerant. 
        Decrease it to make points cleaner

    Returns
    -------
    pd.DataFrame
        The DataFrame you passed in, with one more point tagged

    """
    # if the point column does not exist, make it
    if(not "point" in df.columns):
        df = df.assign(point = np.full_like(df['tick'].values, -1))
        
    # select a 10s window around the input point and average it
    est_delta = np.timedelta64(5, 's')
    df_tmp = chop_time(df, timestamp-est_delta, timestamp+est_delta)
    start_lat = np.mean(df_tmp["Lat"].values)
    start_lon = np.mean(df_tmp["Lon"].values)
    start_hmsl= np.mean(df_tmp["hmsl"].values)
    
    # find the starting location
    init_index = 0
    for stamp in df['timestamp'].values:
        if stamp < timestamp:
            init_index += 1
        else: break
    
    # find the left edge of the point
    left_offset = 0
    while(init_index - left_offset > 0):
        ind = init_index - left_offset
        lat = df['Lat'].values[ind]
        lon = df['Lon'].values[ind]
        hmsl= df['hmsl'].values[ind]
        
        dist = geopy.distance.distance((start_lat, start_lon), (lat, lon)).m
        if (dist < thresh): left_offset += 1
        else: break
    left_index = init_index - left_offset
    print(df['timestamp'].values[left_index])
    
    # find the right edge of the point
    right_offset = 0
    while(init_index + right_offset < len(df['tick'].values)-1):
        ind = init_index + right_offset
        lat = df['Lat'].values[ind]
        lon = df['Lon'].values[ind]
        hmsl= df['hmsl'].values[ind]
        
        dist = geopy.distance.distance((start_lat, start_lon), (lat, lon)).m
        if (dist < thresh): right_offset += 1
        else: break
    right_index = init_index + right_offset
    print(df['timestamp'].values[right_index])
    
    points_in = df['point'].values
    this_point = np.max(points_in) + 1
    print(np.max(points_in))
    for i in range(left_index, right_index):
        points_in[i] = this_point
    df = df.assign(point = points_in)

    return(df)

# output the points into an NPZ
def output_points(df, name):
    """
    Outputs a list of points to an NPZ file

    Parameters
    ----------
    df : pd.DataFrame
        The DataFrame to output
    name : String
        The name of the dataset, for naming the output file

    Notes
    -----
    Saves data to ./npz/name_points.npz

    """
    mean_alt, std_alt, mean_az, std_az = [],[],[],[]
    
    for i in range(max(df['point'].values)):
        az = df[df['point'] == i]['az'].values
        el = df[df['point'] == i]['el'].values
        #ang = np.subtract(90, el)
        #for j in range(len(az)):
        #    if(az[j] > 180):
        #        az[j] = np.subtract(az[j], 180)
        #        ang[j] = np.multiply(-1., ang[j])
        mean_alt.append(np.mean(el))
        mean_az.append(np.mean(az))
        std_alt.append(np.std(el))
        std_az.append(np.std(az))

    np.savez("./npz/"+name+"_points.npz", mean_alt=mean_alt, std_alt=std_alt, mean_az=mean_az, std_az=std_az)
    print("Saved NPZ")



def find_points(df, start_time, end_time, min_len=np.timedelta64(500, 'ms')):
    # make a point column if it doesn't exist
    if(not "point" in df.columns):
        df = df.assign(point = np.full_like(df['tick'].values, -1))

    # find the starting point number
 
    # read in the currently used point values and 
    points_in = df['point'].values
    next_point = np.max(points_in) + 1
                

    # chop the dataframe into the requested range
    df_cut = chop_time(df, start_time, end_time)

    last_stat_time = start_time
    was_moving = True
    for row in df_cut.itertuples(index=True):
        if was_moving == True and row.isMoving == False:
            # just started a point
            last_stat_time = row.timestamp # set the beginning of the point to now
            was_moving = False # note that we are no longer moving
        elif was_moving == False and row.isMoving == False:
            # in the middle of a point
            continue # do nothing
        elif was_moving == False and row.isMoving == True:
            # ending a point
            # check if we've been still for long enough
            if row.timestamp - last_stat_time >= min_len:
                # this is a valid point, tag it
                # find the left and right indicies of the point, in the main df
                print(np.datetime64(last_stat_time, 'ns'))
                print(np.datetime64(row.timestamp))
                print(df[df['timestamp']>=last_stat_time]['timestamp'].values[0])
                left_index = np.where(df['timestamp'].values==np.datetime64(last_stat_time,'ns'))[0][0]
                right_index = np.where(df['timestamp'].values==np.datetime64(row.timestamp,'ns'))[0][0]
                print(left_index)
                print(right_index)
                # fill in the point number
                for j in range(left_index, right_index):
                    points_in[j] = next_point
                df = df.assign(point = points_in)
                next_point += 1

            was_moving = True # note that we're moving again

    return(df)





def point_plot(df, point, figsize=(10,10)):
    df_slice = df[df["point"] == point]
    lat_arr = df_slice["Lat"].values
    lon_arr = df_slice["Lon"].values

    # Histogram of each coordinate
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.hist(lat_arr)
    ax2.hist(lon_arr)
    
    




