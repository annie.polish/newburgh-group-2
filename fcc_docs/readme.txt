| part                      | model    | FCC ID         |
| ------------------------- | -------- | -------------- |
| Drone                     | M600P    | SS3-PM8201512  |
| Controller                | GL858A   | SS3-GL658C1504 |
| Datalink Pro 900 (flight) | DL900ANA | SS3-ZT300A1604 |
| Datalink Pro 900 (ground) | DL900GNA | SS3-ZT300G1604 |
| LightBridge 2             | R810A    | SS3-IG8101508  |
