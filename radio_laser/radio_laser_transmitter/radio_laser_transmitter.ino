
/*
  Annie Polish
  Summer 2019
  Based on TMRh20's example program

  Transmitter module for controlling a drone-mounted laser
*/

#include <SPI.h>
#include "RF24.h"

bool radioNumber = 1; //TRANSMITTER

/* Hardware configuration: Set up nRF24L01 radio on SPI bus */
RF24 radio(4, 10);
/**********************************************************/

byte addresses[][6] = {"1Node", "2Node"};

// Used to control whether this node is sending or receiving
bool role = radioNumber; 

void setup() {
  Serial.begin(115200);

  // set pins for a switch and indicator lights
  pinMode(15, INPUT_PULLUP);
  pinMode(16, OUTPUT);
  pinMode(17, OUTPUT);
  digitalWrite(16, HIGH);
  digitalWrite(17, HIGH);
  Serial.println("I'm role 1");

  radio.begin();

  // Open a writing and reading pipe on each radio, with opposite addresses
  radio.openWritingPipe(addresses[1]);
  radio.openReadingPipe(1, addresses[0]);

  // Start the radio listening for data
  radio.startListening();
}

void loop() {

  /****************** Ping Out Role ***************************/

  radio.stopListening();                                    // First, stop listening so we can talk.


  Serial.println(F("Now sending"));

  bool laser_state = digitalRead(15);
  if (laser_state == true) {
    Serial.println("switch on");
  } else {
    Serial.println("switch off");
  }

  int laser_packet = 10 + int(laser_state)*10;
  Serial.println(laser_packet);

  Serial.println(radio.isChipConnected());

  unsigned long start_time = micros(); // Take the time, and send it.  This will block until complete
  Serial.print(start_time);
  if (!radio.write( &laser_packet, sizeof(int) )) {
    Serial.println(F("failed"));
    digitalWrite(16, HIGH);
  }

  radio.startListening();                                    // Now, continue listening

  unsigned long started_waiting_at = micros();               // Set up a timeout period, get the current microseconds
  boolean timeout = false;                                   // Set up a variable to indicate if a response was received or not

  while ( ! radio.available() ) {                            // While nothing is received
    if (micros() - started_waiting_at > 100000 ) {           // If waited longer than 200ms, indicate timeout and exit while loop
      timeout = true;
      digitalWrite(16, HIGH);
      break;
    }
  }

  if ( timeout ) {                                            // Describe the results
    Serial.println(F("Failed, response timed out."));
    digitalWrite(16, HIGH);
  } else {
    int packet_back;                               // Grab the response, compare, and send to debugging spew
    radio.read( &packet_back, sizeof(int) );
    if (packet_back == 0) {
      digitalWrite(16, HIGH); // indicate failed link
    } else {
      digitalWrite(16, LOW); // indicate successful link
      if (packet_back == 20) {
        digitalWrite(17, LOW); //laser is on, so turn LED on
      } else {
        digitalWrite(17, HIGH);
      }
    }
    unsigned long end_time = micros();

    // Spew it
    Serial.print(F("Sent "));
    Serial.print(start_time);
    Serial.print(F(", Got response "));
    Serial.print(packet_back);
    Serial.print(F(", Round-trip delay "));
    Serial.print(end_time - start_time);
    Serial.println(F(" microseconds"));
  }

  // Try again 100ms later
  delay(100);
}
