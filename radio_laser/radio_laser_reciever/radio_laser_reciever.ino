
/*
  Annie Polish
  Summer 2019
  Based on TMRh20's example program

  Reciever module for controlling a drone-mounted laser
*/


#include <SPI.h>
#include "RF24.h"

bool radioNumber = 0; //RECIEVER

/* Hardware configuration: Set up nRF24L01 radio on SPI bus */
RF24 radio(4, 10);
/**********************************************************/

byte addresses[][6] = {"1Node", "2Node"};

// Used to control whether this node is sending or receiving
bool role = radioNumber;

// teensy interval timer object, used for dead man switch
IntervalTimer myTimer;
#define TIMEOUT 500 //millis before we kill the laser
bool man_dead = false; //is the man dead??
int keep_alive = TIMEOUT; //global to decrement

void setup() {
  Serial.begin(115200);

  // set pins for a single output (to a power transistor)
  pinMode(15, OUTPUT);
  digitalWrite(15, LOW);
  Serial.println("I'm role 0");
  radio.begin();

  // Open a writing and reading pipe on each radio, with opposite addresses
  radio.openWritingPipe(addresses[0]);
  radio.openReadingPipe(1, addresses[1]);

  // Start the radio listening for data
  radio.startListening();

  // Start the safety timer
  myTimer.begin(deadMan, 1000);
}

void loop() {


  /****************** Pong Back Role ***************************/

  int packet_in;
  Serial.println("role 0, waiting");
  Serial.println(radio.isChipConnected());

  if ( radio.available()) {
    // Variable for the received timestamp
    while (radio.available()) {                                   // While there is data ready
      radio.read( &packet_in, sizeof(int) );             // Get the payload
    }

    if (packet_in == 10) { // good packet, turn laser off
      digitalWrite(15, LOW);
      keep_alive = TIMEOUT; // reset the timer
      man_dead = false;
    } else if (packet_in == 20) { // good packet, turn laser on
      digitalWrite(15, HIGH);
      keep_alive = TIMEOUT; // reset the timer
      man_dead = false;
    } else {
      digitalWrite(15, LOW);
    }

    radio.stopListening();                                        // First, stop listening so we can talk
    radio.write( &packet_in, sizeof(unsigned long) );              // Send the final one back.
    radio.startListening();                                       // Now, resume listening so we catch the next packets.
    Serial.print(F("Sent response "));
    Serial.println(packet_in);
  }

  // activate the dead-man switch
  if (man_dead) {
    digitalWrite(15, LOW);
  }
}

void deadMan() {
  keep_alive--;
  if(keep_alive < 0) {
    man_dead = true;
  }
}
