Annie Polish
Summer 2019


This directory contains arduino scripts for a remote-conrolled laser.
It was mounted under the drone and used for indicating its center.
The main feature of these scripts is that the laser shuts off
when it looses radio contact.

One script is for the arduino on the drone, and one is for a ground transmitter

It expects to run on a Teensy 3.2 with a NRF24L01 radio module.
